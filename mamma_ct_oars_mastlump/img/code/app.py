import json
import os.path

from monai.deploy.core import Application

from ops.op_postprocessing import SeparateStructure
from ops.op_inference import Predict
from ops.op_reader import DataLoader
from ops.op_writer import DataWriter
from ops.op_select_inference import Select_Predict
from ops.op_lr_classifier import LR_Classifier
from ops.op_mast_lump_classifier import MastLumpClassify

class InferenceApplication(Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compose(self):
        dataloader_op = DataLoader()

        meta_info_517NT = self.load_meta_info("meta_info_517_NT.json")
        meta_info_518MastR = self.load_meta_info("meta_info_518_MastR.json")
        meta_info_519MastL = self.load_meta_info("meta_info_519_MastL.json")
        meta_info_521LumpR = self.load_meta_info("meta_info_521_LumR.json")
        meta_info_522LumpL = self.load_meta_info("meta_info_522_LumL.json")
        meta_info_MastLumpStructures = self.load_meta_info("meta_info_MastLumpStructures.json")

        predict_517NT_op = Predict(task=meta_info_517NT)
        LR_classifier_op = LR_Classifier(task=meta_info_517NT, model=meta_info_MastLumpStructures["LR_model"])
        
        predict_Mast_op = Select_Predict(first_task=meta_info_519MastL, second_task=meta_info_518MastR,
                                         first_task_key="left", second_task_key="right")
        predict_Lump_op = Select_Predict(first_task=meta_info_522LumpL, second_task=meta_info_521LumpR,
                                         first_task_key="left", second_task_key="right")
        
        mast_lump_classifier_op = MastLumpClassify(split_labels=meta_info_MastLumpStructures["split_labels"], model=meta_info_MastLumpStructures["MastLump_model"])

        postprocessing_NT_op = SeparateStructure(split_labels=meta_info_517NT["split_labels"])
        postprocessing_seg_op = SeparateStructure(split_labels=meta_info_MastLumpStructures["split_labels"])

        writer_NT_op = DataWriter(meta_info=meta_info_517NT, description=meta_info_517NT["id"], output_file=None)
        writer_seg_op = DataWriter(meta_info=meta_info_MastLumpStructures, description=meta_info_MastLumpStructures["id"], output_file="rtstruct_predictions.dcm")

        # Flows
        self.add_flow(dataloader_op, predict_517NT_op, {"image": "img"})

        # Skip if L or R in tag
        # self.add_flow(dataloader_op, LR_classifier_op, {"series_description": "series_description"})
        self.add_flow(dataloader_op, LR_classifier_op, {"image": "image"})
        self.add_flow(predict_517NT_op, LR_classifier_op, {"seg": "NT_img"})

        # Skip if Lump in tag
        self.add_flow(dataloader_op, predict_Mast_op, {"image": "img"})
        self.add_flow(LR_classifier_op, predict_Mast_op, {"side": "task_key"})

        # Skip if Mast in tag
        self.add_flow(dataloader_op, predict_Lump_op, {"image": "img"})
        self.add_flow(LR_classifier_op, predict_Lump_op, {"side": "task_key"})

        # self.add_flow(dataloader_op, mast_lump_classifier_op, {"series_description": "series_description"})
        self.add_flow(dataloader_op, mast_lump_classifier_op, {"image": "img"})
        self.add_flow(predict_Mast_op, mast_lump_classifier_op, {"seg": "mast_seg"})
        self.add_flow(predict_Lump_op, mast_lump_classifier_op, {"seg": "lump_seg"})

        self.add_flow(predict_517NT_op, postprocessing_NT_op, {"seg": "img"})

        self.add_flow(mast_lump_classifier_op, postprocessing_seg_op, {"seg": "img"})
        # self.add_flow(mast_lump_classifier_op, postprocessing_seg_op, {"seg_meta": "seg_meta"})

        self.add_flow(dataloader_op, writer_NT_op, {"dcm_dir": "dcm_dir"})
        self.add_flow(postprocessing_NT_op, writer_NT_op, {"pp_img": "img"})

        self.add_flow(dataloader_op, writer_seg_op, {"dcm_dir": "dcm_dir"})
        self.add_flow(postprocessing_seg_op, writer_seg_op, {"pp_img": "img"})
        self.add_flow(writer_NT_op, writer_seg_op, {"out_struct": "rtstruct"})


    @staticmethod
    def load_meta_info(path):
        with open(os.path.join('/cfg', path), "r") as r:#/home/frog/mathis/segmentation-models/mamma_ct_oars_mastlump/img
            meta_info = json.loads(r.read())
        return meta_info

if __name__ == "__main__":
    InferenceApplication(do_run=True)
