#!/bin/bash

if [ "$#" -lt 2 ]; then
    echo "Please provide an input and output folder as arguments. 
    Optional arguments: 
    a build stage {dev, prod}
    a docker image tag (e.g. 0.3). Default: latest
    'push' as last argument to push the image to dockerhub. Will also push to :latest
    Example: ./local_build_and_run.sh /path/to/input /path/to/output dev 0.3 push"
    exit 1
elif [ "$#" -lt 3 ]; then
    echo "No build stage specified, using 'prod'"
    export stage="prod"
else
    export stage=$3
fi
if [[ -z "$4" ]]; then
    echo "No tag specified, using 'latest'"
    export tag="latest"
else
    export tag=$4
fi

DOCKER_BUILDKIT=1 \
    docker build \
    --target $stage\
    --build-arg BUILDKIT_INLINE_CACHE=1 \
    -t dcpt/seg_hn_ct_oars_ka:$tag \
    img/

docker run \
  -it \
  --gpus=all \
  --ipc=host \
  -v $(realpath $1):/input \
  -v $(realpath $2):/output \
  dcpt/seg_hn_ct_oars_ka:$tag



if [[ "$5" = "push" ]]; then
    echo "Pushing image to dockerhub"
    docker tag dcpt/seg_hn_ct_oars_ka:latest dcpt/seg_hn_ct_oars_ka:$tag
    docker push dcpt/seg_hn_ct_oars_ka:latest
    docker push dcpt/seg_hn_ct_oars_ka:$tag
fi