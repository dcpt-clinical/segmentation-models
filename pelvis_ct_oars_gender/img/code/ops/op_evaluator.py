import pydicom
import logging
import os
from typing import List
import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, InputContext, IOType, Operator, OutputContext
from nnunet.evaluation.surface_dice import normalized_surface_dice
from nnunet.evaluation.metrics import dice, hausdorff_distance_95, ConfusionMatrix
from monai.deploy.utils.importutil import optional_import
import numpy as np

# Optional imports to facilitate testing
unittest, _ = optional_import('unittest')
timing, _ = optional_import('ops.timer')
import json


@md.input("seg", dict, IOType.IN_MEMORY)
@md.input("folds", dict[np.ndarray], IOType.IN_MEMORY)
@md.input("dcm_dir", str, IOType.IN_MEMORY)
@md.output("evals", dict, IOType.DISK)
@md.env(pip_packages=["monai==0.6.0", "simpleitk", "numpy", "nnunet"])
class Evaluator(Operator):
    """Evaluates the segmentation results. Calculates dice, surface dice, and hausdorff distance for each structure.
    Receives the post-processed arrays in a dict: pp_arrs[task_id][[prediction][folds]].
    Returns a dict with the mean metrics for each structure: evals[task_id][structure_id][metric].
    """

    def __init__(self,
                 meta_info: dict):
        self.task = meta_info
        self.logger = logging.getLogger(f"{__name__}.{type(self).__name__}")
        if os.environ['BUILD_ENV'] == "dev":
            self.logger.setLevel(logging.DEBUG)
        super().__init__()

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        self.timer = timing.TimeOP(__name__)
        prediction = op_input.get("seg")
        folds = op_input.get("folds")
        self.spacing = self.get_spacing(op_input.get("dcm_dir"))

        evals_out = dict()
        # for task in self.tasks:
            #Load evals from disk for debugging
            # if os.environ['BUILD_ENV'] == "dev":
            #     evals_out[task['id']] = json.load(open(os.path.join(os.environ['VALIDATION'], "evals_mean.json"), "r"))[task['id']]
            #     continue
        task = self.task

        if len(task["folds"]) > 1:
            self.logger.info(f"Evaluating task: {task['id']}")

            post_vals = self.get_unique_structures(prediction)
            
            evals = self.evaluate_prediction(prediction, folds, post_vals, task['surface_dice_threshold'])
            
            # Consider representing evals as something other than a mean value
            evals_out[task['id']] = self.get_mean_metrics(evals, task, post_vals)

        else:
            self.logger.info(f"Skipping evaluation for task: {task['id']} since it has only one fold")
            evals_out[task['id']] = None

        op_output.set(evals_out, "evals")

        print(self.timer.report())

    def evaluate_prediction(self, prediction: np.ndarray, prediction_folds: dict, post_vals: list, sDSC_threshold: float) -> dict:
        evals = dict()
        evals = {
            str(structure_id):
                self.evaluate_structure(
                    prediction, prediction_folds, structure_id, sDSC_threshold)
                for structure_id in post_vals
        }
        return evals

    def get_mean_metrics(self, evals: dict, task_info: dict, post_vals: list) -> dict:
        mean_evals = {
            str(structure_id):
            {
                metric: np.mean([evals[str(structure_id)][metric][fold_id]
                                    for fold_id in task_info["folds"]])
                for metric in ["s_DSC", "dice", "hd95"]
            }
            for structure_id in post_vals
        }
        return mean_evals

    # Calculates metrics for a single structure across all folds in arr_folds_dict
    def evaluate_structure(self, arr: np.ndarray, arr_folds_dict: dict, struct_id: int, sDSC_threshold: float) -> dict:
        metrics = {'dice': dict(), 's_DSC': dict(), 'hd95': dict()}
        arr = self.to_binary(arr, struct_id)

        for fold_id, arr_fold in arr_folds_dict.items():

            metrics['dice'][fold_id], metrics['s_DSC'][fold_id], metrics['hd95'][fold_id] = self.evaluate_fold(
                arr, arr_fold, struct_id, sDSC_threshold)

            self.logger.debug(
                f"Structure: {struct_id}, Fold: {fold_id}, dice: {round(metrics['dice'][fold_id], 5)}, s_DSC: {round(metrics['s_DSC'][fold_id], 5)}, hd95: {round(metrics['hd95'][fold_id], 5)}")

        return metrics

    # Calculates metrics for a single structure in a single fold
    # Uses nnUNET's functions
    def evaluate_fold(self, arr: np.ndarray, arr_fold: np.ndarray, struct_id: int, sDSC_threshold: float) -> tuple:
        arr_fold = self.to_binary(arr_fold, struct_id)
        confusion_matrix = ConfusionMatrix(test=arr_fold, reference=arr)
        fold_dice = dice(
            test=arr_fold, reference=arr, confusion_matrix=confusion_matrix)
        fold_s_DSC = normalized_surface_dice(
            arr_fold, arr, threshold=sDSC_threshold, spacing=self.spacing)
        fold_hd95 = hausdorff_distance_95(
            test=arr_fold, reference=arr, voxel_spacing=self.spacing, confusion_matrix=confusion_matrix)

        return fold_dice, fold_s_DSC, fold_hd95

    # Returns a list of all unique values in arr, excluding 0
    def get_unique_structures(self, arr: np.ndarray) -> list:
        return np.unique(arr)[1:]

    @staticmethod
    def to_binary(arr: np.ndarray, structure_id: int):
        return np.where(arr == structure_id, 1, 0)

    # Reads the spacing from the first dcm file in the directory
    # Assumes the first dimension is slices. For cns, this ususally means a shape of (360, 480, 480)
    def get_spacing(self, dcm_dir) -> tuple:
        for file in os.listdir(dcm_dir):
            if file.endswith(".dcm") or file.startswith("IM_"):
                dcm = pydicom.dcmread(os.path.join(dcm_dir, file))
                return (float(dcm.SpacingBetweenSlices), float(dcm.PixelSpacing[0]), float(dcm.PixelSpacing[1]))
        else:
            self.logger.error(f"No dcm file found in {dcm_dir}")

class Testing(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        import os
        from pathlib import Path
        import json
        import numpy as np

        current_file_dir = Path(__file__).parent.resolve()
        validation_dir = current_file_dir.joinpath("../TestData/ValidationData")
        output_dir = current_file_dir.joinpath("../TestData/Results")
        meta_path = current_file_dir.joinpath("../../cfg/meta_info_504.json")

        # Load the meta info
        with open(meta_path, "r") as r:
            meta_info = json.loads(r.read())

        # Load the post-processed arrays
        prediction = np.load(os.path.join(validation_dir, "504_sep.npy"))
        folds = {str(fold_id): np.load(os.path.join(validation_dir, f"504_fold_{fold_id}_sep.npy")) for fold_id in range(5)}

        # Load the spacing
        s_DSC_threshold = 2.0
        spacing = (0.5, 0.52083331346511, 0.52083331346511)

        # Create an evaluator
        evaluator = Evaluator(meta_info=meta_info)
        evaluator.spacing = spacing

        # Call evaluator methods
        cls.post_vals = evaluator.get_unique_structures(prediction)

        # Load the expected results
        evals_expected = json.load(open(os.path.join(validation_dir, "evals.json"), "r"))
        evals_expected_mean = json.load(open(os.path.join(validation_dir, "evals_mean.json"), "r"))
        cls.evals_expected_504 = evals_expected["504"]
        cls.evals_expected_mean_504 = evals_expected_mean["504"]
        
        # Calculate the metrics
        pred_bin_1 = evaluator.to_binary(prediction, 1)
        cls.evals_mean = evaluator.get_mean_metrics(cls.evals_expected_504, meta_info, cls.post_vals)
        cls.strct_1_dice, cls.strct_1_s_DSC, cls.strct_1_hd95 = evaluator.evaluate_fold(pred_bin_1, folds["0"], 1, s_DSC_threshold)

    def test_get_unique_structures(self):
        self.assertTrue(set([1, 2, 3, 4, 5, 6, 7, 8, 9]).issuperset(set(self.post_vals)))

    def test_dice(self):
        self.assertAlmostEqual(self.strct_1_dice, self.evals_expected_504["1"]["dice"]["0"], delta=0.0001)

    def test_s_DSC(self):
        self.assertAlmostEqual(self.strct_1_s_DSC, self.evals_expected_504["1"]["s_DSC"]["0"], delta=0.0001)

    def test_hd95(self):
        self.assertAlmostEqual(self.strct_1_hd95, self.evals_expected_504["1"]["hd95"]["0"], delta=0.0001)

    def test_mean(self):
        ((self.assertAlmostEqual(self.evals_mean[structure_id][metric], 
                                self.evals_expected_mean_504[structure_id][metric])
            for metric in self.evals_expected_mean_504[structure_id].keys())
            for structure_id in self.evals_expected_mean_504.keys())

if __name__ == "__main__":
    unittest.main()