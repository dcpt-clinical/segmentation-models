import logging
import SimpleITK as sitk
import numpy as np

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, DataPath, InputContext, IOType, Operator, OutputContext
from .timer import TimeOP


@md.input("", DataPath, IOType.DISK)
@md.output("image", dict, IOType.IN_MEMORY)
# @md.output("image", sitk.Image, IOType.IN_MEMORY)
# @md.output("series_description", str, IOType.IN_MEMORY)
@md.output("dcm_dir", str, IOType.IN_MEMORY)
@md.env(pip_packages=["monai==0.6.0", "simpleitk", "numpy"])
class DataLoader(Operator):
    '''
    Reads the dicom series from the input directory and returns the image and the input directory.
    The image is returned as a dict with the series description as the key and the image as the value.
    This is a workaround for the fact that the monai deploy fails to output both the image and the description as separate outputs.
    '''
    def __init__(self):
        self.logger = logging.getLogger(f"{__name__}.{type(self).__name__}")
        super().__init__()

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        timer = TimeOP(__name__)
        in_path = str(op_input.get("").path)

        reader = sitk.ImageSeriesReader()
        reader.LoadPrivateTagsOn()
        dicom_names = reader.GetGDCMSeriesFileNames(in_path)
        reader.SetFileNames(dicom_names)

        series_description = self.getSeriesDescription(dicom_names)

        img = reader.Execute()

        # This is a workaround for the fact that the monai deploy fails to output both the image and the description as separate outputs
        # So instead we output a dict with both the image and the description
        # Should be fixed in the future
        outimage = {series_description : img}#md.Image(sitk.GetArrayFromImage(img), metadata=series_description)
        op_output.set(value=outimage, label="image")
        # op_output.set(value=series_description, label="series_description")
        op_output.set(value=in_path, label="dcm_dir")
        print(timer.report())

    def getSeriesDescription(self, dicom_names):
        meta_reader = sitk.ImageFileReader()
        meta_reader.SetFileName(dicom_names[0])
        meta_reader.ReadImageInformation()
        series_description = meta_reader.GetMetaData('0008|103e')
        return series_description