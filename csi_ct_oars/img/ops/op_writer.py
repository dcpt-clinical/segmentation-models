import logging
import os
import shutil
from typing import Dict
from contextlib import redirect_stdout

import monai.deploy.core as md
import numpy as np
from monai.deploy.core import ExecutionContext, DataPath, InputContext, IOType, Operator, OutputContext
from rt_utils import RTStructBuilder

from .timer import TimeOP


@md.input("511", np.ndarray, IOType.IN_MEMORY)
@md.input("512", np.ndarray, IOType.IN_MEMORY)
@md.input("513", np.ndarray, IOType.IN_MEMORY)
@md.input("514", np.ndarray, IOType.IN_MEMORY)
@md.input("dcm_dir", str, IOType.IN_MEMORY)
@md.output("", DataPath, IOType.DISK)
@md.env(pip_packages=["monai==0.6.0", "simpleitk", "numpy"])
class DataWriter(Operator):
    def __init__(self,
                 meta_511_info: Dict,
                 meta_512_info: Dict,
                 meta_513_info: Dict,
                 meta_514_info: Dict):

        self.logger = logging.getLogger("{}.{}".format(__name__, type(self).__name__))
        super().__init__()
        self.meta_info_511 = meta_511_info
        self.meta_info_512 = meta_512_info
        self.meta_info_513 = meta_513_info
        self.meta_info_514 = meta_514_info

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        timer = TimeOP(__name__)

        dcm_dir = op_input.get("dcm_dir")

        arr_511 = op_input.get("511")
        arr_512 = op_input.get("512")

        # Transpose like in postprocessing
        arr_513 = op_input.get("513")
        arr_513 = np.transpose(arr_513, (2, 0, 1))

        arr_514 = op_input.get("514")
        arr_514 = np.transpose(arr_514, (2, 0, 1))


        rtstruct = RTStructBuilder.create_new(dicom_series_path=dcm_dir)

        for meta_arr in [(self.meta_info_511, arr_511),
                         (self.meta_info_512, arr_512),
                         (self.meta_info_513, arr_513),
                         (self.meta_info_514, arr_514)]:
            meta, arr = meta_arr
            self.add_contours_to_rtstruct(pred_arr=arr, rtstruct=rtstruct, final_contours=meta["final_contours"])

        out_path = op_output.get().path
        rtstruct.save(os.path.join(out_path, "rtstruct_predictions.dcm"))

        # pass input dcm series through.
        self.copytree(dcm_dir, out_path)

        print(timer.report())

    def add_contours_to_rtstruct(self, pred_arr, rtstruct, final_contours):
        arr = np.transpose(pred_arr, (2, 0, 1))

        for i, detail in final_contours.items():
            i = int(i)
            mask = self.get_boolean_array(arr, i)
            with redirect_stdout(None): # Ignore excessive output from rtstruct functions
                if True in mask:
                    rtstruct.add_roi(
                        mask=mask,
                        # color=detail["color"],
                        name=detail["name"]
                    )

    @staticmethod
    def get_boolean_array(pred_array: np.array, label_i: int):
        return pred_array == label_i

    @staticmethod
    def copytree(src, dst, symlinks=False, ignore=None):
        for item in os.listdir(src):
            s = os.path.join(src, item)
            d = os.path.join(dst, item)
            if os.path.isdir(s):
                shutil.copytree(s, d, symlinks, ignore)
            else:
                shutil.copy2(s, d)
