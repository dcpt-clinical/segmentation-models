Starting... 
2023-02-13 16:05:05.394186: Using splits from existing split file: /data/jeskal/projects/nnUNet/nnUNet_preprocessed/Task521_T_Mamma_LumR/splits_final.pkl 
2023-02-13 16:05:05.495356: The split file contains 5 splits. 
2023-02-13 16:05:05.495534: Desired fold for training: 3 
2023-02-13 16:05:05.501167: This split has 43 training and 11 validation cases. 
2023-02-13 16:05:05.664838: TRAINING KEYS:
 odict_keys(['AIDCPTATLAS_T_Br_011', 'AIDCPTATLAS_T_Br_015', 'AIDCPTATLAS_T_Br_016', 'AIDCPTATLAS_T_Br_020', 'AIDCPTATLAS_T_Br_021', 'AIDCPTATLAS_T_Br_044', 'AIDCPTATLAS_T_Br_045', 'AIDCPTATLAS_T_Br_062', 'AIDCPTATLAS_T_Br_106', 'AIDCPTATLAS_T_Br_117', 'AIDCPTATLAS_T_Fliped_Br_018', 'AIDCPTATLAS_T_Fliped_Br_023', 'AIDCPTATLAS_T_Fliped_Br_025', 'AIDCPTATLAS_T_Fliped_Br_028', 'AIDCPTATLAS_T_Fliped_Br_029', 'AIDCPTATLAS_T_Fliped_Br_034', 'AIDCPTATLAS_T_Fliped_Br_038', 'AIDCPTATLAS_T_Fliped_Br_039', 'AIDCPTATLAS_T_Fliped_Br_041', 'AIDCPTATLAS_T_Fliped_Br_042', 'AIDCPTATLAS_T_Fliped_Br_047', 'AIDCPTATLAS_T_Fliped_Br_048', 'AIDCPTATLAS_T_Fliped_Br_052', 'AIDCPTATLAS_T_Fliped_Br_055', 'AIDCPTATLAS_T_Fliped_Br_057', 'AIDCPTATLAS_T_Fliped_Br_058', 'AIDCPTATLAS_T_Fliped_Br_059', 'AIDCPTATLAS_T_Fliped_Br_068', 'AIDCPTATLAS_T_Fliped_Br_069', 'AIDCPTATLAS_T_Fliped_Br_070', 'AIDCPTATLAS_T_Fliped_Br_073', 'AIDCPTATLAS_T_Fliped_Br_077', 'AIDCPTATLAS_T_Fliped_Br_082', 'AIDCPTATLAS_T_Fliped_Br_083', 'AIDCPTATLAS_T_Fliped_Br_084', 'AIDCPTATLAS_T_Fliped_Br_092', 'AIDCPTATLAS_T_Fliped_Br_096', 'AIDCPTATLAS_T_Fliped_Br_098', 'AIDCPTATLAS_T_Fliped_Br_100', 'AIDCPTATLAS_T_Fliped_Br_101', 'AIDCPTATLAS_T_Fliped_Br_103', 'AIDCPTATLAS_T_Fliped_Br_108', 'AIDCPTATLAS_T_Fliped_Br_118']) 
2023-02-13 16:05:05.664966: VALIDATION KEYS:
 odict_keys(['AIDCPTATLAS_T_Br_061', 'AIDCPTATLAS_T_Br_067', 'AIDCPTATLAS_T_Br_078', 'AIDCPTATLAS_T_Br_105', 'AIDCPTATLAS_T_Br_110', 'AIDCPTATLAS_T_Br_113', 'AIDCPTATLAS_T_Fliped_Br_008', 'AIDCPTATLAS_T_Fliped_Br_010', 'AIDCPTATLAS_T_Fliped_Br_017', 'AIDCPTATLAS_T_Fliped_Br_040', 'AIDCPTATLAS_T_Fliped_Br_099']) 
2023-02-13 16:05:08.913742: loading checkpoint /data/jeskal/projects/nnUNet/nnUNet_trained_models/nnUNet/3d_fullres/Task521_T_Mamma_LumR/nnUNetTrainerV2__nnUNetPlansv2.1/fold_3/model_latest.model train= True 
2023-02-13 16:05:10.408656: lr: 0.009095 
2023-02-13 16:05:44.893836: Unable to plot network architecture: 
2023-02-13 16:05:44.917997: No module named 'hiddenlayer' 
2023-02-13 16:05:44.918087: 
printing the network instead:
 
2023-02-13 16:05:44.918133: Generic_UNet(
  (conv_blocks_localization): ModuleList(
    (0): Sequential(
      (0): StackedConvLayers(
        (blocks): Sequential(
          (0): ConvDropoutNormNonlin(
            (conv): Conv3d(640, 320, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
            (instnorm): InstanceNorm3d(320, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
            (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
          )
        )
      )
      (1): StackedConvLayers(
        (blocks): Sequential(
          (0): ConvDropoutNormNonlin(
            (conv): Conv3d(320, 320, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
            (instnorm): InstanceNorm3d(320, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
            (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
          )
        )
      )
    )
    (1): Sequential(
      (0): StackedConvLayers(
        (blocks): Sequential(
          (0): ConvDropoutNormNonlin(
            (conv): Conv3d(512, 256, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
            (instnorm): InstanceNorm3d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
            (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
          )
        )
      )
      (1): StackedConvLayers(
        (blocks): Sequential(
          (0): ConvDropoutNormNonlin(
            (conv): Conv3d(256, 256, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
            (instnorm): InstanceNorm3d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
            (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
          )
        )
      )
    )
    (2): Sequential(
      (0): StackedConvLayers(
        (blocks): Sequential(
          (0): ConvDropoutNormNonlin(
            (conv): Conv3d(256, 128, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
            (instnorm): InstanceNorm3d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
            (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
          )
        )
      )
      (1): StackedConvLayers(
        (blocks): Sequential(
          (0): ConvDropoutNormNonlin(
            (conv): Conv3d(128, 128, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
            (instnorm): InstanceNorm3d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
            (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
          )
        )
      )
    )
    (3): Sequential(
      (0): StackedConvLayers(
        (blocks): Sequential(
          (0): ConvDropoutNormNonlin(
            (conv): Conv3d(128, 64, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
            (instnorm): InstanceNorm3d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
            (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
          )
        )
      )
      (1): StackedConvLayers(
        (blocks): Sequential(
          (0): ConvDropoutNormNonlin(
            (conv): Conv3d(64, 64, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
            (instnorm): InstanceNorm3d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
            (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
          )
        )
      )
    )
    (4): Sequential(
      (0): StackedConvLayers(
        (blocks): Sequential(
          (0): ConvDropoutNormNonlin(
            (conv): Conv3d(64, 32, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
            (instnorm): InstanceNorm3d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
            (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
          )
        )
      )
      (1): StackedConvLayers(
        (blocks): Sequential(
          (0): ConvDropoutNormNonlin(
            (conv): Conv3d(32, 32, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
            (instnorm): InstanceNorm3d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
            (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
          )
        )
      )
    )
  )
  (conv_blocks_context): ModuleList(
    (0): StackedConvLayers(
      (blocks): Sequential(
        (0): ConvDropoutNormNonlin(
          (conv): Conv3d(1, 32, kernel_size=(1, 3, 3), stride=(1, 1, 1), padding=(0, 1, 1))
          (instnorm): InstanceNorm3d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
          (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
        )
        (1): ConvDropoutNormNonlin(
          (conv): Conv3d(32, 32, kernel_size=(1, 3, 3), stride=(1, 1, 1), padding=(0, 1, 1))
          (instnorm): InstanceNorm3d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
          (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
        )
      )
    )
    (1): StackedConvLayers(
      (blocks): Sequential(
        (0): ConvDropoutNormNonlin(
          (conv): Conv3d(32, 64, kernel_size=(3, 3, 3), stride=(1, 2, 2), padding=(1, 1, 1))
          (instnorm): InstanceNorm3d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
          (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
        )
        (1): ConvDropoutNormNonlin(
          (conv): Conv3d(64, 64, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
          (instnorm): InstanceNorm3d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
          (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
        )
      )
    )
    (2): StackedConvLayers(
      (blocks): Sequential(
        (0): ConvDropoutNormNonlin(
          (conv): Conv3d(64, 128, kernel_size=(3, 3, 3), stride=(2, 2, 2), padding=(1, 1, 1))
          (instnorm): InstanceNorm3d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
          (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
        )
        (1): ConvDropoutNormNonlin(
          (conv): Conv3d(128, 128, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
          (instnorm): InstanceNorm3d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
          (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
        )
      )
    )
    (3): StackedConvLayers(
      (blocks): Sequential(
        (0): ConvDropoutNormNonlin(
          (conv): Conv3d(128, 256, kernel_size=(3, 3, 3), stride=(2, 2, 2), padding=(1, 1, 1))
          (instnorm): InstanceNorm3d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
          (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
        )
        (1): ConvDropoutNormNonlin(
          (conv): Conv3d(256, 256, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
          (instnorm): InstanceNorm3d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
          (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
        )
      )
    )
    (4): StackedConvLayers(
      (blocks): Sequential(
        (0): ConvDropoutNormNonlin(
          (conv): Conv3d(256, 320, kernel_size=(3, 3, 3), stride=(2, 2, 2), padding=(1, 1, 1))
          (instnorm): InstanceNorm3d(320, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
          (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
        )
        (1): ConvDropoutNormNonlin(
          (conv): Conv3d(320, 320, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
          (instnorm): InstanceNorm3d(320, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
          (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
        )
      )
    )
    (5): Sequential(
      (0): StackedConvLayers(
        (blocks): Sequential(
          (0): ConvDropoutNormNonlin(
            (conv): Conv3d(320, 320, kernel_size=(3, 3, 3), stride=(2, 2, 2), padding=(1, 1, 1))
            (instnorm): InstanceNorm3d(320, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
            (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
          )
        )
      )
      (1): StackedConvLayers(
        (blocks): Sequential(
          (0): ConvDropoutNormNonlin(
            (conv): Conv3d(320, 320, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
            (instnorm): InstanceNorm3d(320, eps=1e-05, momentum=0.1, affine=True, track_running_stats=False)
            (lrelu): LeakyReLU(negative_slope=0.01, inplace=True)
          )
        )
      )
    )
  )
  (td): ModuleList()
  (tu): ModuleList(
    (0): ConvTranspose3d(320, 320, kernel_size=(2, 2, 2), stride=(2, 2, 2), bias=False)
    (1): ConvTranspose3d(320, 256, kernel_size=(2, 2, 2), stride=(2, 2, 2), bias=False)
    (2): ConvTranspose3d(256, 128, kernel_size=(2, 2, 2), stride=(2, 2, 2), bias=False)
    (3): ConvTranspose3d(128, 64, kernel_size=(2, 2, 2), stride=(2, 2, 2), bias=False)
    (4): ConvTranspose3d(64, 32, kernel_size=(1, 2, 2), stride=(1, 2, 2), bias=False)
  )
  (seg_outputs): ModuleList(
    (0): Conv3d(320, 13, kernel_size=(1, 1, 1), stride=(1, 1, 1), bias=False)
    (1): Conv3d(256, 13, kernel_size=(1, 1, 1), stride=(1, 1, 1), bias=False)
    (2): Conv3d(128, 13, kernel_size=(1, 1, 1), stride=(1, 1, 1), bias=False)
    (3): Conv3d(64, 13, kernel_size=(1, 1, 1), stride=(1, 1, 1), bias=False)
    (4): Conv3d(32, 13, kernel_size=(1, 1, 1), stride=(1, 1, 1), bias=False)
  )
) 
2023-02-13 16:05:44.920618: 
 
2023-02-13 16:05:44.920736: 
epoch:  100 
2023-02-13 16:14:55.899453: train loss : -0.4715 
2023-02-13 16:15:36.951167: validation loss: -0.4041 
2023-02-13 16:15:37.031568: Average global foreground Dice: [0.8967, 0.9341, 0.0, 0.8521, 0.7137, 0.765, 0.8096, 0.8285, 0.8062, 0.8004, 0.9226] 
2023-02-13 16:15:37.032171: (interpret this as an estimate for the Dice of the different classes. This is not exact.) 
2023-02-13 16:15:37.474907: lr: 0.009086 
2023-02-13 16:15:37.530941: saving checkpoint... 
2023-02-13 16:15:40.522298: done, saving took 3.05 seconds 
2023-02-13 16:15:40.555772: This epoch took 595.634997 s
 
2023-02-13 16:15:40.555869: 
epoch:  101 
