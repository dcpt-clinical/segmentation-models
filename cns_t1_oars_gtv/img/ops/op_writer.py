import logging
import os
import shutil
from typing import Dict
from contextlib import redirect_stdout

import monai.deploy.core as md
import numpy as np
from monai.deploy.core import ExecutionContext, DataPath, InputContext, IOType, Operator, OutputContext
from rt_utils import RTStructBuilder

from .timer import TimeOP


@md.input("seg", np.ndarray, IOType.IN_MEMORY)
@md.input("seg_gtv", np.ndarray, IOType.IN_MEMORY)
@md.input("dcm_dir", str, IOType.IN_MEMORY)
@md.output("", DataPath, IOType.DISK)
@md.env(pip_packages=["monai==0.6.0", "simpleitk", "numpy"])
class DataWriter(Operator):
    def __init__(self, final_contours: Dict):
        self.logger = logging.getLogger("{}.{}".format(__name__, type(self).__name__))
        super().__init__()
        self.final_contours = final_contours

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        timer = TimeOP(__name__)

        arr = op_input.get("seg")
        arr = np.transpose(arr, (2, 0, 1))

        arr_gtv = op_input.get("seg_gtv")
        arr_gtv = np.transpose(arr_gtv, (2, 0, 1)) ## From post processing
        arr_gtv = np.transpose(arr_gtv, (2, 0, 1))

        dcm_dir = op_input.get("dcm_dir")
        out_path = op_output.get().path

        rtstruct = RTStructBuilder.create_new(dicom_series_path=dcm_dir)

        ## Set oars
        for i, detail in self.final_contours.items():
            i = int(i)
            mask = self.get_boolean_array(arr, i)
            with redirect_stdout(None): # Ignore excessive output from rtstruct functions
                if True in mask:
                    rtstruct.add_roi(
                        mask=mask,
                        color=detail["color"],
                        name=detail["name"]
                    )

        # Set GTV
        mask = self.get_boolean_array(arr_gtv, 1)
        if True in mask:
            rtstruct.add_roi(
                mask=mask,
                color=[255, 0, 0],
                name="AI_GTV"
            )

        rtstruct.save(os.path.join(out_path, "rtstruct_predictions.dcm"))
        self.copytree(dcm_dir, out_path)  # pass input dcm series through.
        print(timer.report())

    @staticmethod
    def get_boolean_array(pred_array: np.array, label_i: int):
        return pred_array == label_i

    @staticmethod
    def copytree(src, dst, symlinks=False, ignore=None):
        for item in os.listdir(src):
            s = os.path.join(src, item)
            d = os.path.join(dst, item)
            if os.path.isdir(s):
                shutil.copytree(s, d, symlinks, ignore)
            else:
                shutil.copy2(s, d)
