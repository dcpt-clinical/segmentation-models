import logging
import os
import numpy as np
from skimage import measure
from typing import Tuple, List

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, InputContext, IOType, Operator, OutputContext

from .timer import TimeOP


@md.input("img", np.ndarray, IOType.IN_MEMORY)
@md.input("folds", dict, IOType.IN_MEMORY)
@md.input("meta_info", dict, IOType.IN_MEMORY)
@md.output("pp_img", np.ndarray, IOType.IN_MEMORY)
@md.output("folds", dict, IOType.IN_MEMORY)
@md.env(pip_packages=["monai", "numpy", "nnunet", "skimage"])
class SeparateStructure(Operator):
    '''
    Separates structures for the provided task (split_labels)
    Receives a numpy array of the prediction
    Returns a numpy array with all pre values replaced by their post values and mirrored structures separated into left and right.
    '''
    def __init__(self, split_labels=None):
        self.logger = logging.getLogger(f"{__name__}.{type(self).__name__}")
        super().__init__()
        self.split_labels = split_labels

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):

        timer = TimeOP(__name__)
        if self.split_labels == None :
            self.split_labels = op_input.get("meta_info")["split_labels"]
            
        try:
            folds = op_input.get("folds")
            if folds:
                for fold in folds:
                    folds[fold] = self.add_non_mirrored_structures(self.split_labels["non_mirrored"], folds[fold], folds[fold])
                    folds[fold] = self.add_mirrored_structures(self.split_labels["mirrored"], folds[fold], folds[fold])
                op_output.set(folds, "folds")
        except:
            pass
            
        arr = op_input.get("img")
        try:
            arr_out = np.zeros_like(arr)
            arr_out = self.add_non_mirrored_structures(self.split_labels["non_mirrored"], arr, arr_out)
            arr_out = self.add_mirrored_structures(self.split_labels["mirrored"], arr, arr_out)
            op_output.set(arr_out, "pp_img")
            
            print(timer.report())

        except Exception as e:
            self.logger.error(e)
            raise e

    def add_non_mirrored_structures(self, labels: dict, arr_i: np.ndarray, arr_o: np.ndarray) -> np.ndarray:
        for structure in labels: 
            if structure["pre_val"] in arr_i:
                arr_o = np.where(arr_i == structure["pre_val"], structure["post_val"], arr_o)
            else:  
                self.logger.warning(f'{structure["name"]} was not inferred correctly.')
        return arr_o

    # This method splits structures such as lungs into left and right, since nnUNET does not do this
    # To do this, it finds the two largest regions, and assigns the leftmost to the left structure, and the rightmost to the right structure, based on centroid x coordinate
    def add_mirrored_structures(self, labels: dict, arr_i: np.ndarray, arr_o: np.ndarray) -> np.ndarray:
        for structure in labels: 
            props_sorted, error = self.get_ordered_regions(arr_i, structure["pre_val"], mirrored=True)
            if (error):
                # Discards mirrored organ if two seperate structures cannot be detected
                self.logger.warning(f'{structure["name"]} was not inferred correctly.')
                continue

            # Log centroids
            self.logger.info(f'{structure["name"]} centroids: {props_sorted[0].centroid}, {props_sorted[1].centroid}')
            # Find left/right by centroid x coordinate
            # This piece of code is very important. If the provided image has the wrong orientation, the mirrored structures can be assigned to the wrong side
            # This is also important for the classifiers, but not the remaining code
            # All the models currently transpose the image to the correct orientation, but this mamma model does it earlier in the pipeline than most
            # (In the inference operator, rather than the writer operator)
            # Two possible solutions:
            # 1. Make sure the image is transposed correctly before it is passed to this operator
            # 2. Change the code below to use the largest difference in centroid coordinates, rather than just the 2nd coordinate
            # This would be more robust to images with the wrong orientation, but might cause more carelessness and thus problems for other operators
            # Possible implementation of solution 2, which findes the largest difference in centroid coordinates between the two structures:
            
            R_arr = min([props_sorted[0], props_sorted[1]], key=lambda prop: prop.centroid[1])
            L_arr = max([props_sorted[0], props_sorted[1]], key=lambda prop: prop.centroid[1])

            # Give structures new values
            arr_o = np.where(L_arr._label_image == L_arr.label, structure["post_val"][0], arr_o)
            arr_o = np.where(R_arr._label_image == R_arr.label, structure["post_val"][1], arr_o)

        return arr_o

    # Returns struct properties ordered by area (descending), and error code:
    # 0: At least two region dectected, 1: Less than two regions detected for mirrored structure, 2: No regions detected
    def get_ordered_regions(self, arr: np.ndarray, val: int, mirrored: bool) -> Tuple[List[measure._regionprops.RegionProperties], int]:
        if val not in arr:
            return [np.zeros_like(arr), np.zeros_like(arr)], 2
        arr = np.where(arr == val, 1, 0)
        L = measure.label(arr, connectivity=3) 
        props = measure.regionprops(L)
        # Sort props by size, descending
        props_sorted = sorted(props, key=lambda prop: prop.area, reverse=True)
        if len(props_sorted) >= 2:
            return props_sorted, 0
        elif len(props_sorted) == 1 and mirrored==False:
            return [props_sorted[0]], 0
        elif len(props_sorted) == 1 and mirrored==True:
            return props_sorted, 1
        else:
            return None, 2