#!/bin/bash

run_params=()
if [ "$1" == "-h" ] || [ "$#" -lt 2 ]; then
    echo "This script runs a docker image for the seg_hn_ct_level segmentation model.
    To build the image, run ./build_image.sh.
    Required arguments:
    an input folder
    an output folder
    Optional arguments: 
    a build stage {dev, prod}
    a docker image tag (e.g. 0.3) to run. Default: latest
    Example: ./local_build_and_run.sh /path/to/input /path/to/output dev 0.3"
    exit 1
elif [ "$#" -lt 3 ]; then
    echo "No build stage specified, using 'prod'"
    export stage="prod"
else
    export stage=$3
    echo "Using build stage $stage"
    if [ "$stage" = "dev" ]; then
        echo "Mounting code folder"
        chmod +x img/code/main.sh
        run_params+=(--mount type=bind,source="$(pwd)/img/code",target=/code)
        run_params+=(--env BUILD_ENV=dev)
        run_params+=(--env ENV_SAVE_NII=1)
    fi
fi
if [[ -z "$4" ]]; then
    echo "No tag specified, using 'latest'"
    export tag="latest"
else
    export tag=$4
fi

docker run \
  -it \
  --gpus=all \
  --ipc=host \
   "${run_params[@]}" \
  -v $(realpath $1):/input \
  -v $(realpath $2):/output \
  dcpt/seg_hn_ct_level:$tag