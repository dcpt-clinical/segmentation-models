import logging
import os
import shutil
from typing import List
from contextlib import redirect_stdout
import SimpleITK as sitk
import numpy as np
from rt_utils import RTStructBuilder

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, DataPath, InputContext, IOType, Operator, OutputContext

from .timer import TimeOP


# @md.input("descriptions", dict, IOType.IN_MEMORY)
@md.input("pp_arrs", dict, IOType.IN_MEMORY)
@md.input("dcm_dir", str, IOType.IN_MEMORY)
@md.input("evals", dict, IOType.IN_MEMORY)
@md.output("", DataPath, IOType.DISK)
@md.env(pip_packages=["monai==0.6.0", "simpleitk", "numpy"])
class DataWriter(Operator):
    '''
    Writes the post-processed arrays to disk as rtstructs in the dcm_dir and copies the input dcm_dir to the output dir.
    Writes the evals to structure descriptions in the rtstruct.    
    '''
    def __init__(self,
                 meta_info : List[dict]):
        self.logger = logging.getLogger(f"{__name__}.{__name__}")
        if os.environ['BUILD_ENV'] == "dev":
            self.logger.setLevel(logging.DEBUG)
        super().__init__()
        self.meta_info = meta_info

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        timer = TimeOP(__name__)

        dcm_dir = op_input.get("dcm_dir")
        evals = op_input.get("evals")
        arrs = op_input.get("pp_arrs")
        
        if os.environ["ENV_SAVE_NII"] == "1":
            # Merges all predictions into one array
            arrs_out = [arrs[task['id']]['prediction'] for task in self.meta_info]
            arr_out = arrs_out[0]
            for array in arrs_out[1:]:
                arr_out = np.where(arr_out != 0, arr_out, array)
            sitk.WriteImage(sitk.GetImageFromArray(arr_out), 
                            os.path.join(os.environ["OUTPUT"], 
                            "post_processed_mask.nii.gz"))

        rtstruct = RTStructBuilder.create_new(dicom_series_path=dcm_dir)
        for meta in self.meta_info:
            self.add_contours_to_rtstruct(
                pred_arr=arrs[meta["id"]]['prediction'], 
                rtstruct=rtstruct, 
                final_contours=meta["final_contours"], 
                evals=evals[meta["id"]],
                model_name=meta["model_name"])

        out_path = op_output.get().path
        rtstruct.save(os.path.join(out_path, "rtstruct_predictions.dcm"))

        # Temporary fix for lowercase "Body Part Examined" tag error.
        # Should be replaced by copytree() call after root cause is fixed
        self.copy_with_new_tag(dcm_dir, out_path)
        # self.copytree(dcm_dir, out_path)  # pass input dcm series through.
        
        print(timer.report())

    def add_contours_to_rtstruct(self, pred_arr, rtstruct, final_contours, evals, model_name):
        arr = np.transpose(pred_arr, (1, 2, 0))
        for i, detail in final_contours.items():
            if evals is None:
                description = f"Model: {model_name}"
            else:
                description = (
                    f"Model: {model_name}, "
                    f"s_DSC: {round(evals[f'{i}']['s_DSC'], 5)}, " 
                    f"Dice: {round(evals[f'{i}']['dice'], 5)}, " 
                    f"hd95: {round(evals[f'{i}']['hd95'], 5)}"
                    )
            i = int(i)
            mask = self.get_boolean_array(arr, i)
            # Ignore excessive output from rtstruct functions
            with redirect_stdout(None):
                if True in mask:
                    rtstruct.add_roi(
                        mask=mask,
                        color=detail["color"],
                        name=detail["name"],
                        description=description
                    )

    @staticmethod
    def get_boolean_array(pred_array: np.array, label_i: int):
        return pred_array == label_i

    @staticmethod
    def copytree(src, dst, symlinks=False, ignore=None):
        for item in os.listdir(src):
            s = os.path.join(src, item)
            d = os.path.join(dst, item)
            if os.path.isdir(s):
                shutil.copytree(s, d, symlinks, ignore)
            else:
                shutil.copy2(s, d)

    # Changes Body Part Examined tag and saves to output dir
    def copy_with_new_tag(self, src, dst):
        for dcm in os.listdir(src):
            try:
                reader = sitk.ImageFileReader()
                reader.LoadPrivateTagsOn()
                reader.SetFileName(os.path.join(src, dcm))
                img = reader.Execute()
                img.SetMetaData(
                    "0018|0015", img.GetMetaData("0018|0015").upper())
                writer = sitk.ImageFileWriter()
                writer.KeepOriginalImageUIDOn()
                if dcm.endswith('.dcm'):
                    writer.SetFileName(os.path.join(dst, dcm))
                else:
                    writer.SetFileName(os.path.join(dst, dcm + '.dcm'))
                writer.Execute(img)
            except RuntimeError as rte:
                self.logger.warning(
                    f"Error while attempting to change tag of {dcm}.")
                shutil.copy2(os.path.join(src, dcm), os.path.join(dst, dcm))
