pydicom==2.1.2
monai
monai-deploy-app-sdk
torch>=1.6
numpy
simpleitk
typeguard<=2.13.3 # Version 3.0.0 is not compatible with monai
matplotlib
rt_utils
scikit-image
highdicom
