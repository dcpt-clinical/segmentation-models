#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Please provide an input and output folder as arguments"
    exit 1
fi

docker run \
  -it \
  --gpus=all \
  --ipc=host \
  -v $(realpath $1):/input \
  -v $(realpath $2):/output \
  dcpt/seg_cns_t1_oars_gtv:latest