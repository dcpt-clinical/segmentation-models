#!/bin/bash

if [ "$1" == "-h" ]; then
    echo "Build a docker image for the seg_mamma_ct_oars_mastlump segmentation model.
    Optional arguments: 
    a docker image tag (e.g. 0.3). Default: latest
    Example: ./build_image.sh 0.3"
    exit 1
fi

if [[ -z "$1" ]]; then
    echo "No tag specified, using 'latest'"
    export tag="latest"
else
    export tag=$1
fi

DOCKER_BUILDKIT=1 \
    docker build \
    --target prod \
    --build-arg BUILDKIT_INLINE_CACHE=1 \
    -t dcpt/seg_mamma_ct_oars_targets:$tag \
    img/