import logging
import os
import pickle
from typing import Dict
import numpy as np
import SimpleITK as sitk

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, InputContext, IOType, Operator, OutputContext

from .timer import TimeOP
#Output:
    #0: Right
    #1: Left
@md.input("image", dict, IOType.IN_MEMORY)
# @md.input("series_description", str, IOType.IN_MEMORY)
@md.input("NT_img", np.ndarray, IOType.IN_MEMORY)
@md.output("side", str, IOType.IN_MEMORY)
class LR_Classifier(Operator):
    def __init__(self, task: dict, model: str):
        self.logger = logging.getLogger(f"{__name__}.{type(self).__name__}")
        super().__init__()
        self.task = task
        self.model = model

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        timer = TimeOP(__name__)
        NT_img=op_input.get("NT_img")
        img = op_input.get("image")

        series_description = next(iter(img.keys()))#img.metadata()
        img = list(img.values())[0]#img.asnumpy()
        img = sitk.GetArrayFromImage(img)
        img = np.transpose(img, (1, 2, 0))##!!!!

        if "left" in series_description.lower():
            self.logger.info("Selecting left side based on series description")
            side = "left"
        elif "right" in series_description.lower():
            self.logger.info("Selecting right side based on series description")
            side = "right"
        else:
            predic = self.Determine_LR(NT_img, img, series_description)

            if predic==0:
                side = "right"
                self.logger.info("Detected side: Right")
            elif predic==1:
                side = "left"
                self.logger.info("Detected side: Left")
    
        op_output.set(side, "side")
        print(timer.report())

    def Determine_LR(self, NT_img, img, series_description):
        struct = self.open_separate(NT_img)

        for k in struct.keys():
                # struct[k] = np.transpose(struct[k], (2, 1, 0))
            for z in range(np.shape(struct[k])[2]):
                struct[k][:,:,z]=struct[k][:,:,z].T

        for z in range(np.shape(img)[2]):
            img[:,:,z]=img[:,:,z]

        if 'struct5' in struct.keys():
            big_z=self.biggest_slice(struct['struct5'])
            xt=self.row_in_z(struct['struct5'], big_z)            
            tall=int(xt[-1])

        if ('struct5'  not in struct):
            fl=np.zeros((np.shape(img)[2],2))     
            for z in range(np.shape(img)[2]):
                cl=[]
                for y in range(np.shape(img)[1]):
                    if np.count_nonzero(img[y,:350,z]>-900)!=0:
                        cl.append(y)
                fl[z,0]=cl[0]
                fl[z,1]=cl[-1]
            zm=int(np.shape(img)[2]/2)
            tall=int((fl[zm,0]+fl[zm,1])/2)
        li=[]    
        for j in range(1,10):
            img2 = np.transpose(img, (1, 0, 2))
            img_c=np.flip(img2[tall-100 : tall+100, : 300,np.shape(img)[2]-j],axis=1)
            # img_c=np.flip(img[ :300, tall-100 : tall+100, np.shape(img)[2]-j],axis=0)#np.flip(img[tall-100:tall+100,:300,np.shape(img)[2]-j],axis=1)
            img_r=(img_c>0)*1
            b=(img_r!=0).nonzero()

            x=b[0]

            y=b[1]
            mod1=np.poly1d(np.polyfit(x,y,1))
            
            li.append(np.rad2deg(np.arctan(mod1[1])))
        ang=np.mean(li)
                
        model1=pickle.load(open(self.model, 'rb'))
            
        predic=model1.predict(np.array(ang).reshape(-1,1))
        return predic
    
    def open_separate(self, data: np.ndarray) -> Dict[str, np.ndarray]:
        """

        Create dictionary with binary arrays for each unique element in input array

        Parameters:
        data (numpy.ndarray): 3D numpy array

        Returns:
        dict[numpy.ndarray]: dict of binary arrays with key 'struct#' where # is a value from the input array

        """

        res = np.unique(data)

        structs_dict = {f"struct{int(i)}" : np.where(data == i, i, 0) for i in res}

        return structs_dict
    
    @staticmethod
    def to_binary(arr: np.ndarray, structure_id: int):
        return np.where(arr == structure_id, 1, 0)


    @staticmethod
    def biggest_slice(structure: np.ndarray):
        # Find the slice index with the most non-zero values in the z direction
        max_slice_index = np.argmax(np.count_nonzero(structure, axis=(0, 1)))
        
        return max_slice_index

    import numpy as np

    @staticmethod
    def row_in_z(structure, z):
        # Initialize variables
        max_size = 0
        indices = []
        
        # Iterate over the specified row or column
        for i in range(structure.shape[1]):
            slice_size = np.count_nonzero(structure[i, :, z])
            if slice_size > 0:
                indices.append(i)
                if slice_size > max_size:
                    max_size = slice_size
        
        return indices

if __name__ == '__main__':
    os.environ['BUILD_ENV'] = 'dev'
    class_op = LR_Classifier(task={"eqwsa": 5}, model='/home/frog/mathis/segmentation-models/mamma_ct_oars_mastlump/img/model/LumpMast_Classifier/forest.sav')

    img = np.transpose(sitk.GetArrayFromImage(sitk.ReadImage('/home/frog/docker_test/out_mamma/scan.nii.gz')), (1, 2, 0))
    # import nibabel as nib
    # img = nib.load('/home/frog/docker_test/out_mamma/scan517.nii.gz').get_fdata()
    NT_img = sitk.GetArrayFromImage(sitk.ReadImage('/home/frog/docker_test/out_mamma/scan517.nii.gz'))
    img_lump = sitk.GetArrayFromImage(sitk.ReadImage('/home/frog/docker_test/out_mamma/scan521.nii.gz'))

    class_op.Determine_LR(NT_img, img, 'test')