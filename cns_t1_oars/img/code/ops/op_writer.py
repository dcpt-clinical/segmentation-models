import logging
import os
import shutil
from typing import Dict
from contextlib import redirect_stdout
import SimpleITK as sitk
import numpy as np
from rt_utils import RTStructBuilder

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, DataPath, InputContext, IOType, Operator, OutputContext

from .timer import TimeOP


@md.input("504", np.ndarray, IOType.IN_MEMORY)
@md.input("509", np.ndarray, IOType.IN_MEMORY)
@md.input("dcm_dir", str, IOType.IN_MEMORY)
@md.output("", DataPath, IOType.DISK)
@md.env(pip_packages=["monai==0.6.0", "simpleitk", "numpy"])
class DataWriter(Operator):
    def __init__(self,
                 meta_info_504: Dict,
                 meta_info_509: Dict):
        self.logger = logging.getLogger(
            "{}.{}".format(__name__, type(self).__name__))
        super().__init__()
        self.meta_info_504 = meta_info_504
        self.meta_info_509 = meta_info_509

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        timer = TimeOP(__name__)

        dcm_dir = op_input.get("dcm_dir")
        arr_504 = op_input.get("504")
        arr_509 = op_input.get("509")
        
        if os.environ["ENV_SAVE_NII"] == "1":
            import SimpleITK as sitk
            # Merges the two arrays into one
            arr_out = np.where(arr_504 > 0, arr_504, arr_509)
            sitk.WriteImage(sitk.GetImageFromArray(arr_out), 
                            os.path.join(os.environ["OUTPUT"], 
                            "post_processed_mask.nii.gz"))

        rtstruct = RTStructBuilder.create_new(dicom_series_path=dcm_dir)
        for meta_arr in [(self.meta_info_504, arr_504),
                         (self.meta_info_509, arr_509)]:
            meta, arr = meta_arr
            self.add_contours_to_rtstruct(
                pred_arr=arr, rtstruct=rtstruct, final_contours=meta["final_contours"])

        out_path = op_output.get().path
        rtstruct.save(os.path.join(out_path, "rtstruct_predictions.dcm"))

        # Temporary fix for lowercase "Body Part Examined" tag error.
        # Should be replaced by copytree() call after root cause is fixed
        self.copy_with_new_tag(dcm_dir, out_path)
        # self.copytree(dcm_dir, out_path)  # pass input dcm series through.
        print(timer.report())

    def add_contours_to_rtstruct(self, pred_arr, rtstruct, final_contours):
        arr = np.transpose(pred_arr, (1, 2, 0))
        # arr = np.transpose(pred_arr, (2, 1, 0))
        for i, detail in final_contours.items():
            i = int(i)
            mask = self.get_boolean_array(arr, i)
            # Ignore excessive output from rtstruct functions
            with redirect_stdout(None):
                if True in mask:
                    rtstruct.add_roi(
                        mask=mask,
                        color=detail["color"],
                        name=detail["name"]
                    )

    @staticmethod
    def get_boolean_array(pred_array: np.array, label_i: int):
        return pred_array == label_i

    @staticmethod
    def copytree(src, dst, symlinks=False, ignore=None):
        for item in os.listdir(src):
            s = os.path.join(src, item)
            d = os.path.join(dst, item)
            if os.path.isdir(s):
                shutil.copytree(s, d, symlinks, ignore)
            else:
                shutil.copy2(s, d)

    # Changes Body Part Examined tag and saves to output dir
    def copy_with_new_tag(self, src, dst):
        for dcm in os.listdir(src):
            try:
                reader = sitk.ImageFileReader()
                reader.LoadPrivateTagsOn()
                reader.SetFileName(os.path.join(src, dcm))
                img = reader.Execute()
                img.SetMetaData(
                    "0018|0015", img.GetMetaData("0018|0015").upper())
                writer = sitk.ImageFileWriter()
                writer.KeepOriginalImageUIDOn()
                if dcm.endswith('.dcm'):
                    writer.SetFileName(os.path.join(dst, dcm))
                else:
                    writer.SetFileName(os.path.join(dst, dcm + '.dcm'))
                writer.Execute(img)
            except RuntimeError as rte:
                self.logger.warning(
                    f"Error while attempting to change tag of {dcm}.")
                shutil.copy2(os.path.join(src, dcm), os.path.join(dst, dcm))
