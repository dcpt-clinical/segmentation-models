import os
from ops.op_inference import Predict

import logging
import SimpleITK as sitk
import numpy as np

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, InputContext, IOType, OutputContext

from .timer import TimeOP

@md.input("img", dict, IOType.IN_MEMORY)
@md.input("task_key", str, IOType.IN_MEMORY)
@md.output("seg", np.ndarray, IOType.IN_MEMORY)
class Select_Predict(Predict):
    '''Selects the correct prediction task based on the input task_key.
    
    init args:
        first_task (dict): First task option
        second_task (dict): Second task option
        first_task_key (str): Key to use to select the first task
        second_task_key (str): Key to use to select the second task
        
    input args:
        img (sitk.Image): Image to predict on
        task_key (str): Key to use to select the correct task

    output:
        prediction (np.ndarray): Prediction array
        '''
    def __init__(self, first_task: dict, second_task: dict,
                 first_task_key: str = "left", second_task_key: str = "right"):
        self.logger = logging.getLogger(f"{__name__}.{type(self).__name__}")
        super().__init__()

        self.first_task = first_task
        self.second_task = second_task
        self.first_task_key = first_task_key
        self.second_task_key = second_task_key

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        timer = TimeOP(__name__)
        task_key = op_input.get("task_key")
        img = op_input.get("img")#.asnumpy()
        img = list(img.values())[0]
        # img = next(iter(img.values()))#sitk.GetImageFromArray(img)
        if task_key.lower() == self.first_task_key:
            self.logger.info(f"Using task {self.first_task['id']}, based on task_key {task_key}")
            self.task = self.first_task
        elif task_key.lower() == self.second_task_key:
            self.logger.info(f"Using task {self.second_task['id']}, based on task_key {task_key}")
            self.task = self.second_task
        else:
            raise Exception(f"Unknown task key {task_key}")
        # self.logger.info(f"Shape of input image: {img.GetSize()}")
        # try:
        #     pred_arr = sitk.GetArrayFromImage(sitk.ReadImage(os.path.join(os.environ["OUTPUT"], f"scan{self.task['id']}.nii.gz")))
        # except:
        pred_arr, folds = self.predict(img, task_id=self.task["id"], threshold=self.task["threshold"], folds=self.task["folds"], inference_args=self.task["inference_args"])
            # sitk.WriteImage(sitk.GetImageFromArray(pred_arr), os.path.join(os.environ["OUTPUT"], f"scan{self.task['id']}.nii.gz"))
        

        # self.logger.info(f"Shape of output prediction: {pred_arr.shape}")
        op_output.set(pred_arr, "seg")
        
        # if self.task["folds"] > 1:
        #     op_output.set(folds, "folds")

        print(timer.report())