import logging
import os
import shutil
from typing import Dict
from contextlib import redirect_stdout
import numpy as np
from rt_utils import RTStructBuilder, RTStruct

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, DataPath, InputContext, IOType, Operator, OutputContext
#from monai.deploy.exceptions import ItemNotExistsError
from .timer import TimeOP


@md.input("seg", np.ndarray, IOType.IN_MEMORY)
@md.input("dcm_dir", str, IOType.IN_MEMORY)
@md.output("", DataPath, IOType.DISK)
@md.env(pip_packages=["monai==0.6.0", "simpleitk", "numpy"])
class DataWriter(Operator):
    def __init__(self, final_contours: Dict, description: str = None):
        self.logger = logging.getLogger("{}.{}".format(__name__, type(self).__name__))
        super().__init__()
        self.final_contours = final_contours
        self.description = description #this added to include version information / description

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        timer = TimeOP(__name__)

        arr = op_input.get("seg")

        dcm_dir = op_input.get("dcm_dir")
        out_path = op_output.get().path

        rtstruct = RTStructBuilder.create_new(dicom_series_path=dcm_dir)
        arr = np.transpose(arr, (1, 2, 0))
        for i, detail in self.final_contours.items():
            i = int(i)
            mask = self.get_boolean_array(arr, i)
            # Ignore excessive output from rtstruct functions
            with redirect_stdout(None):
                if True in mask:
                    rtstruct.add_roi(
                        mask=mask,
                        color=detail["color"],
                        name=detail["name"],
                        description=self.description
                    )
        rtstruct.ds.Manufacturer = "DCPT"
        #rtstruct.ds.StructureSetLabel = "AI HN_LEVELS" #should probably be moved to a more high level
        rtstruct.ds.InstitutionName = "DCPT"      
        print("Have changed the institution name to "+  rtstruct.ds.InstitutionName)       
        rtstruct.set_series_description("AI AUH_DCPT HeadNeck v1.0")
        rtstruct.save(os.path.join(out_path, "rtstruct_predictions.dcm"))
        self.copytree(dcm_dir, out_path)  # pass input dcm series through.
        print(timer.report())

    @staticmethod
    def get_boolean_array(pred_array: np.array, label_i: int):
        return pred_array == label_i

    @staticmethod
    def copytree(src, dst, symlinks=False, ignore=None):
        for item in os.listdir(src):
            s = os.path.join(src, item)
            d = os.path.join(dst, item)
            if os.path.isdir(s):
                shutil.copytree(s, d, symlinks, ignore)
            else:
                shutil.copy2(s, d)
