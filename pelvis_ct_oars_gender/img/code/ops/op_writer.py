import logging
import os
import shutil
from typing import Dict
from contextlib import redirect_stdout
import SimpleITK as sitk
import numpy as np
from rt_utils import RTStructBuilder, RTStruct

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, DataPath, InputContext, IOType, Operator, OutputContext
from monai.deploy.exceptions import ItemNotExistsError


from .timer import TimeOP


@md.input("img", np.ndarray, IOType.IN_MEMORY)
@md.input("dcm_dir", str, IOType.IN_MEMORY)
@md.input("meta_info", dict, IOType.IN_MEMORY)
@md.input("rtstruct", RTStruct, IOType.IN_MEMORY)
@md.output("out_struct", RTStruct, IOType.IN_MEMORY)
# @md.output("out_path", DataPath, IOType.DISK)
@md.env(pip_packages=["monai==0.6.0", "simpleitk", "numpy"])
class DataWriter(Operator):
    '''
    Writes the post-processed arrays to disk as rtstructs in the dcm_dir and copies the input dcm_dir to the output dir.
    Writes the evals to structure descriptions in the rtstruct.
    Can be used both as a leaf operator and as an intermediate operator. 
    Default is leaf operator, meaning the struct file is written to disk.
    If the output_file is None, the rtstruct is not written to disk, but returned as an output to the next operator.
    '''
    def __init__(self,
                 meta_info: Dict = None,
                 description: str = None,
                 output_file: str = "rtstruct_predictions"):
        self.logger = logging.getLogger(
            "{}.{}".format(__name__, type(self).__name__))
        super().__init__()
        self.meta_info = meta_info
        self.description = description
        if output_file and len(str(output_file)) > 0:
            if not output_file.endswith(".dcm"):
                output_file = output_file + ".dcm"
            self._output_file = output_file 
        else:
            self._output_file = None

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        timer = TimeOP(__name__)

        if self.meta_info == None :
            self.meta_info = op_input.get("meta_info")

        self.description = self.meta_info["model_name"]
        arr = op_input.get("img")
        dcm_dir = op_input.get("dcm_dir")
        if os.environ["ENV_SAVE_NII"] == "1":
            import SimpleITK as sitk
            try:
                sitk.WriteImage(sitk.GetImageFromArray(np.transpose(arr, (2, 0, 1))), os.path.join(os.environ["OUTPUT"], f"post_processed_mask_{self.meta_info['id']}.nii.gz"))
            except Exception as e:
                self.logger.error(f"Error writing nii: {e}")

        try:
            rtstruct = op_input.get("rtstruct")
        except ItemNotExistsError:
            rtstruct = RTStructBuilder.create_new(dicom_series_path=dcm_dir)
        
        self.add_contours_to_rtstruct(
            arr=arr, rtstruct=rtstruct, final_contours=self.meta_info["final_contours"])

        if self._output_file and len(str(self._output_file)) > 0:
            out_path = context.output.get().path
            _output_file = out_path / self._output_file
            # _output_file.parent.mkdir(parents=True, exist_ok=True)
            self.logger.info(f"Output will be saved in file {_output_file}.")
            rtstruct.save(os.path.join(_output_file))
            self.copytree(dcm_dir, out_path)
        else:
            op_output.set(rtstruct, "out_struct")

        print(timer.report())

    def add_contours_to_rtstruct(self, arr, rtstruct, final_contours):
        for i, detail in final_contours.items():
            i = int(i)
            mask = self.get_boolean_array(arr, i)
            # Ignore excessive output from rtstruct functions
            with redirect_stdout(None):
                if True in mask:
                    rtstruct.add_roi(
                        mask=mask,
                        color=detail["color"],
                        name=detail["name"],
                        description=self.description
                    )

    @staticmethod
    def get_boolean_array(pred_array: np.array, label_i: int):
        return pred_array == label_i

    @staticmethod
    def copytree(src, dst, symlinks=False, ignore=None):
        for item in os.listdir(src):
            s = os.path.join(src, item)
            d = os.path.join(dst, item)
            if os.path.isdir(s):
                shutil.copytree(s, d, symlinks, ignore)
            else:
                shutil.copy2(s, d)
