import logging
import os
import pickle
from typing import Dict
import numpy as np
import SimpleITK as sitk
import pydicom

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, InputContext, IOType, Operator, OutputContext

from .timer import TimeOP
#Output:
    #meta_info dict based on gender

@md.input("dcm_dir", str, IOType.IN_MEMORY)
@md.output("meta_info", dict, IOType.IN_MEMORY)
class gender_Classifier(Operator):
    def __init__(self, male_task: dict, female_task: dict):
        self.logger = logging.getLogger(f"{__name__}.{type(self).__name__}")
        super().__init__()
        self.male_task = male_task
        self.female_task = female_task

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        timer = TimeOP(__name__)
        dcm_dir=op_input.get("dcm_dir")
        gender = self.get_gender(dcm_dir)
        if gender == "male":
            op_output.set(self.male_task)
        elif gender == "female":
            op_output.set(self.female_task)    

    def get_gender(self, dcm_dir) -> tuple:
        for file in os.listdir(dcm_dir):
            if file.endswith(".dcm") or file.startswith("IM_"):
                dcm = pydicom.dcmread(os.path.join(dcm_dir, file))
                try: 
                    CPR_even_uneven = int(dcm.PatientID[-1])
                except ValueError:
                    self.logger.warning(f"Patient gender set to female as default - unrecognized patientID format {dcm.PatientID}")
                    return "female"

                if CPR_even_uneven % 2 ==0:
                    return "female"
                else:    
                    return "male"
        else:
            self.logger.error(f"No dcm file found in {dcm_dir}")    
        

if __name__ == '__main__':
    pass