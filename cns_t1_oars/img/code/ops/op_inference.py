import logging
import os
import tempfile
from typing import List
import SimpleITK as sitk
import numpy as np

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, InputContext, IOType, Operator, OutputContext

from .timer import TimeOP


@md.input("img", sitk.Image, IOType.IN_MEMORY)
@md.output("504", np.ndarray, IOType.IN_MEMORY)
@md.output("509", np.ndarray, IOType.IN_MEMORY)
@md.env(pip_packages=["monai", "simpleitk", "numpy", "nnunet"])
class Predict(Operator):
    def __init__(self, tasks: List[List]):
        self.logger = logging.getLogger(f"{__name__}.{type(self).__name__}")
        super().__init__()
        self.tasks = tasks

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        timer = TimeOP(__name__)
        img = op_input.get("img")

        for task in self.tasks:
            task_id, threshold = task
            if os.environ['BUILD_ENV'] == "dev":
                sitk.WriteImage(img, os.path.join(os.environ["OUTPUT"], "scan.nii.gz"))
                pred_arr = np.load(os.path.join(os.environ['VALIDATION'], task_id + '_pred.npy'))
            else:
                pred_arr = self.predict(img, task_id=task_id, threshold=threshold)
            op_output.set(pred_arr, task_id)
        print(timer.report())

    def predict(self, img, task_id, threshold):
        with tempfile.TemporaryDirectory() as tmp_in, tempfile.TemporaryDirectory() as tmp_out:
            sitk.WriteImage(img, os.path.join(tmp_in, "tmp_0000.nii.gz"))
            if threshold:
                bin_img = self.make_binary_image(img, 130, 1500)
                sitk.WriteImage(bin_img, os.path.join(tmp_in, "tmp_0001.nii.gz"))

            os.system(f"nnUNet_predict -t {task_id} -tr nnUNetTrainerV2 -f 0 -i {tmp_in} -o {tmp_out}")

            for f in os.listdir(tmp_out):
                if f.endswith(".nii.gz"):
                    pred_img = sitk.ReadImage(os.path.join(tmp_out, f))
                    pred_img.CopyInformation(img)
                    pred_arr = sitk.GetArrayFromImage(pred_img)
                    return pred_arr

            else:
                raise Exception("No prediction found")

    def make_binary_image(self, img: sitk.Image, lower_thresh, upper_thresh):
        arr = sitk.GetArrayFromImage(img)
        bin_arr = np.where(np.logical_and(np.greater_equal(arr, lower_thresh), np.greater_equal(upper_thresh, arr)), 1,
                           0)
        bin_img = sitk.GetImageFromArray(bin_arr)
        bin_img.CopyInformation(img)
        return bin_img
