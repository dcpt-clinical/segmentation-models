#!/bin/bash

if [ "$1" == "-h" ]; then
    echo "This script builds a docker image for the seg_cns_t1_oars segmentation model
    and pushes it to dockerhub for the specified tag and :latest.
    Required arguments:
    A tag for the image for dockerhub (e.g. 0.1.0)
    Example: ./local_build_and_push.sh 0.1.0
    "
if [[ -z "$1" ]]; then
    echo "Please provide a tag for the image for dockerhub (e.g. 0.1.0)"
    exit 1
else
    export tag=$1
fi

DOCKER_BUILDKIT=1 \
    docker build \
    --target prod\
    --build-arg BUILDKIT_INLINE_CACHE=1 \
    -t dcpt/seg_cns_t1_oars:$tag \
    img/

read -p "Are you sure you want to push to dockerhub with tag :$1 and :latest? " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
echo "Pushing image to dockerhub"
docker tag dcpt/seg_cns_t1_oars:latest dcpt/seg_cns_t1_oars:$tag
docker push dcpt/seg_cns_t1_oars:$tag
docker push dcpt/seg_cns_t1_oars:latest
fi