import json
import os.path

from monai.deploy.core import Application

from ops.op_postprocessing import SeparateStructure
from ops.op_inference import Predict
from ops.op_reader import DataLoader
from ops.op_writer import DataWriter


class InferenceApplication(Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compose(self):
        dataloader_op = DataLoader()
        meta_info = self.load_meta_info("meta_info.json")
        meta_info_504 = self.load_meta_info("meta_info_504.json")
        meta_info_509 = self.load_meta_info("meta_info_509.json")

        predict_op = Predict(tasks=meta_info["tasks"])
        postprocessing_504_op = SeparateStructure(split_labels=meta_info_504["split_labels"])
        writer_op = DataWriter(meta_info_504=meta_info_504,
                               meta_info_509=meta_info_509)
        # Flows
        self.add_flow(dataloader_op, predict_op, {"img": "img"})
        self.add_flow(predict_op, postprocessing_504_op, {"504": ""})
        self.add_flow(dataloader_op, writer_op, {"dcm_dir": "dcm_dir"})
        self.add_flow(postprocessing_504_op, writer_op, {"": "504"})
        self.add_flow(predict_op, writer_op, {"509": "509"})

    @staticmethod
    def load_meta_info(path):
        with open(os.path.join('/cfg', path), "r") as r:
            meta_info = json.loads(r.read())
        return meta_info

if __name__ == "__main__":
    InferenceApplication(do_run=True)
