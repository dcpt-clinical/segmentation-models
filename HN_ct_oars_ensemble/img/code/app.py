import json
import os.path

from monai.deploy.core import Application

from ops.op_postprocessing import SeparateStructure
from ops.op_inference import Predict
from ops.op_reader import DataLoader
from ops.op_writer import DataWriter

class InferenceApplication(Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compose(self):
        dataloader_op = DataLoader()

        meta_info_515HN = self.load_meta_info("meta_info_515_HN.json")
    
        predict_op = Predict(task=meta_info_515HN)
       
       
        postprocessing_op = SeparateStructure(split_labels=meta_info_515HN["split_labels"])
        writer_op = DataWriter(meta_info=meta_info_515HN, description=meta_info_515HN["model_name"])
        
        # Flows
        self.add_flow(dataloader_op, predict_op, {"image": "img"})
        self.add_flow(predict_op, postprocessing_op, {"seg": "img"})
        self.add_flow(dataloader_op, writer_op, {"dcm_dir": "dcm_dir"})
        self.add_flow(postprocessing_op, writer_op, {"pp_img": "img"})

    @staticmethod
    def load_meta_info(path):
        with open(os.path.join('/cfg', path), "r") as r:
            meta_info = json.loads(r.read())
        return meta_info

if __name__ == "__main__":
    InferenceApplication(do_run=True)
