import json
import os.path

from monai.deploy.core import Application

from ops.op_postprocessing import SeparateStructure
from ops.op_inference import Predict
from ops.op_reader import DataLoader
from ops.op_writer import DataWriter
from ops.op_gender_classifier import gender_Classifier

class InferenceApplication(Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compose(self):
        dataloader_op = DataLoader()

        meta_info_525_Male = self.load_meta_info("meta_info_525_Male.json")
        meta_info_526_Female = self.load_meta_info("meta_info_526_Female.json")
     
        gender_classifier_op = gender_Classifier(male_task=meta_info_525_Male, female_task=meta_info_526_Female)
        
        predict_op = Predict()
        
        postprocessing_op = SeparateStructure()

        writer_op = DataWriter()
        
        # Flows
        self.add_flow(dataloader_op, gender_classifier_op, {"dcm_dir": "dcm_dir"})  #OK
        self.add_flow(dataloader_op, predict_op, {"image": "img"}) #OK
        self.add_flow(gender_classifier_op, predict_op, {"meta_info": "meta_info"})  #OK
        self.add_flow(gender_classifier_op, postprocessing_op, {"meta_info": "meta_info"}) #OK
        self.add_flow(gender_classifier_op, writer_op, {"meta_info": "meta_info"})  #OK

        self.add_flow(predict_op, postprocessing_op, {"seg": "img"})
        self.add_flow(dataloader_op, writer_op, {"dcm_dir": "dcm_dir"})
        self.add_flow(postprocessing_op, writer_op, {"pp_img": "img"})
        

    @staticmethod
    def load_meta_info(path):
        with open(os.path.join('/cfg', path), "r") as r:#/home/frog/mathis/segmentation-models/mamma_ct_oars_mastlump/img
            meta_info = json.loads(r.read())
        return meta_info

if __name__ == "__main__":
    InferenceApplication(do_run=True)
