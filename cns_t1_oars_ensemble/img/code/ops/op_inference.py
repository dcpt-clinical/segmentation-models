import logging
import os
import tempfile
from typing import List
import SimpleITK as sitk
import numpy as np
import shutil

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, InputContext, IOType, Operator, OutputContext

from .timer import TimeOP


@md.input("img", sitk.Image, IOType.IN_MEMORY)
@md.output("predictions", dict, IOType.IN_MEMORY)
@md.env(pip_packages=["monai", "simpleitk", "numpy", "nnunet"])
class Predict(Operator):
    '''
    Operator for inference with nnUNet
    Saves input image to disk and calls nnUNet_predict and nnUNet_ensemble
    After inference, predictions are loaded from disk and returned as a dictionary of numpy arrays with the following structure:
    {
        task_id: {
            prediction: np.ndarray,
            folds: {
                fold_id: np.ndarray
            }
        }
    }
    '''
    def __init__(self, tasks: List[dict]):
        self.logger = logging.getLogger(f"{__name__}.{type(self).__name__}")
        if os.environ['BUILD_ENV'] == "dev":
            self.logger.setLevel(logging.DEBUG)
        super().__init__()
        self.tasks = tasks

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        timer = TimeOP(__name__)
        img = op_input.get("img")

        predictions = {}
        for task in self.tasks:
            predictions[task['id']] = {}

            # Dev build skips inference and loads precomputed predictions
            if os.environ['BUILD_ENV'] == "dev":
                sitk.WriteImage(img, os.path.join(os.environ["OUTPUT"], "scan.nii.gz"))
                predictions[task['id']]['prediction'] = np.load(os.path.join(os.environ['VALIDATION'], task['id'] + '_pred.npy'))
                if len(task['folds']) > 1:
                    predictions[task['id']]['folds'] = {
                        f_id: 
                        np.load(os.path.join(os.environ['VALIDATION'], f'{task["id"]}_fold_{f_id}_pred.npy')) 
                        for f_id 
                        in task['folds']}
                else:
                    predictions[task['id']]['folds'] = None
            else:
                predictions[task['id']]['prediction'], predictions[task['id']]['folds'] = self.predict(img, task_id=task["id"], threshold=task["threshold"], folds=task["folds"])

        op_output.set(predictions, "predictions")
        print(timer.report())

    def predict(self, img, task_id, threshold, folds=None):
        with tempfile.TemporaryDirectory() as tmp_in, tempfile.TemporaryDirectory() as tmp_out:
            sitk.WriteImage(img, os.path.join(tmp_in, "tmp_0000.nii.gz"))
            if threshold:
                bin_img = self.make_binary_image(img, 130, 1500)
                sitk.WriteImage(bin_img, os.path.join(tmp_in, "tmp_0001.nii.gz"))

            if len(folds) == 1:
                self.logger.info(f"Predicting for task {task_id} with threshold {threshold}")
                fold_path = os.path.join(tmp_out, f"Fold_{folds[0]}_results")
                os.makedirs(fold_path, exist_ok=True)
                os.system(f"nnUNet_predict -t {task_id} -tr nnUNetTrainerV2 -f {folds[0]} -i {tmp_in} -o {fold_path} --save_npz")
                return self.read_prediction(fold_path, img), None
            else:
                results = dict()
                fold_paths = []
                for count, fold in enumerate(folds):
                    self.logger.info(f"Predicting fold {fold} (#{count+1} out of {len(folds)}) for task {task_id} with threshold: {threshold}")
                    fold_path = tempfile.mkdtemp(dir=tmp_out, prefix=f"Fold_{fold}_")
                    os.makedirs(fold_path, exist_ok=True)
                    fold_paths.append(fold_path)
                    os.system(f"nnUNet_predict -t {task_id} -tr nnUNetTrainerV2 -f {fold} -i {tmp_in} -o {fold_path} --save_npz")
                    results[fold] = (self.read_prediction(fold_path, img))

                # Ensemble predictions
                ensemble_path = tempfile.mkdtemp(dir=tmp_out, prefix=f"{task_id}_ensemble_results")
                self.logger.info(f"Ensembling {len(folds)} folds for task {task_id} with threshold {threshold}")
                os.system(f"Command:\r\nnnUNet_ensemble -f {' '.join(fold_paths)} -o {ensemble_path}")

                return self.read_prediction(ensemble_path, img), results

    @staticmethod
    def read_prediction(path, img):
        for f in os.listdir(path):
            if f.endswith(".nii.gz"):
                pred_img = sitk.ReadImage(os.path.join(path, f))
                pred_img.CopyInformation(img)
                pred_arr = sitk.GetArrayFromImage(pred_img)
                return pred_arr
        else:
                raise Exception("No prediction found")

    def make_binary_image(self, img: sitk.Image, lower_thresh, upper_thresh):
        arr = sitk.GetArrayFromImage(img)
        bin_arr = np.where(np.logical_and(np.greater_equal(arr, lower_thresh), np.greater_equal(upper_thresh, arr)), 1,
                           0)
        bin_img = sitk.GetImageFromArray(bin_arr)
        bin_img.CopyInformation(img)
        return bin_img