# Segmentation models

This repository contains the model code for DCPTs clinical AI workflow. The actual trained models are stored locally due to their size and will need to merged manually before building.

General documentation of the inference server can be found [here](https://github.com/mathiser/inference_server).

The models are build as docker images, which can be found at https://hub.docker.com/orgs/dcpt/repositories

The inference server API can be found at https://dcpt-frog2.onerm.dk/docs . The currently used models can be accessed here, or directly by going to https://dcpt-frog2.onerm.dk/api/models/

All models use [MONAI Deploy App SDK](https://github.com/Project-MONAI/monai-deploy-app-sdk). The models contain a main file called app.py, which defines the workflow, by instantiating operators and linking their inputs and outputs. 

All operators currently inherit from the base MONAI Operator class, but are otherwise custom. Although the models use very similar operators, there are currently differences, both in operation and input/output.

Metadata files are stored in json, each containing relevant information for a task, such as task id, inferred structure id's, final structure names/colors, etc. This metadata is passed to the operators in app.py upon instantiation.

# Workflow overview
The following schematic shows the workflow for the cns (non-ensemble) model.

![](documentation/SegmentationModelDocker.png "Workflow for CNS")


# Operators

## Primary operators
Generally, the following operators are used:

**op_reader:**

Reads a dicom series from an input directory and outputs it as a sitk.image object. Also outputs the dicom directory path.

**op_inference:**

Runs inference on the image, based on the task info saved in the metadata files. 

This is done by saving the input image in a temporary folder and running nnUNet CLI commands. 
If necessary, threshold images are also created for inference.

After inference, the label files are then read and passed as numpy arrays.

**op_postprocessing:**

Receives the input numpy array from the inference operator and seperates the mirrored structures (such as lungs, optic nerves, etc.), since the nnUNet models usually cannot do this, Mirrored/non-mirrored structures are defined in the metadata files.

Both mirrored and non-mirrored structures are assigned new integer labels. These labels are temporary, and are simply to avoid overlap in labels, as the mirrored structures now have twice as many labels.

The new labels are then passed on as numpy arrays.

**op_writer:**

Receives the post-processed labels and writes the to a dicom struct file, using the names and colors specified in the metadata files. Optionally adds a description to the structures.

## Optional operators

**op_evaluator:**

To be used for ensemble models. Receives the postprocessed labelled images for each fold and ensemble.
The operator then compares each structure in the ensemble image to the corresponding structure in each image fold.
This is done using dice, surface dice and Hausdorff 95 from nnunet.metrics.

**op_select_inference:**

This operator selects a task to use for inference based on an input key. This key is matched to a task when initializing the operator.
By default the operator decides between left and right, but this can be customized in initialization.

The operator is necessary, since monai deploy doesn't seem to provide a way to create conditionals in the app file based on operator run-time outputs. 

So the operator can be initialized and used like this:
```python
from ops.op_select_inference import Select_Predict
...
predict_Mast_op = Select_Predict(first_task=meta_info_519MastL, second_task=meta_info_518MastR,
                                 first_task_key="left", second_task_key="right")
...
self.add_flow(dataloader_op, predict_Mast_op, {"image": "img"})
self.add_flow(LR_classifier_op, predict_Mast_op, {"side": "task_key"})
```

# Docker

Docker is a technology that allows you to create, deploy, and run applications in containers. A **container** is a lightweight and standalone executable package of software that includes everything needed to run an application, such as the code, libraries, and dependencies.

## Docker image and Dockerfile

A **Docker image** is a template or blueprint for creating a container. It's a snapshot of an application's code and dependencies, along with the operating system and runtime environment needed to run it. You can use Docker images to deploy and run an application in any environment that supports Docker.

The docker images are built according to the Dockerfiles contained within each workflow folder.

**Building**

To build docker images, use [`docker build`](https://docs.docker.com/engine/reference/commandline/build/). Build shell scripts can be found in the various models, which take care of this process.

An example Dockerfile might look like this:

```Dockerfile
FROM pytorch/pytorch:2.0.0-cuda11.7-cudnn8-runtime as base
```
A dockerfile starts with a base image - in this case pytorch 2.0. Base images can be found on dockerhub - see [this page](https://hub.docker.com/r/pytorch/pytorch/tags) for pytorch images.

"as base" allows [multi-stage builds](https://docs.docker.com/build/building/multi-stage/), but this is entirely optional.

```Dockerfile
RUN apt-get update && apt-get install -y --no-install-recommends \
    ffmpeg \
    libsm6 \
    libxext6 \
    && rm -rf /var/lib/apt/lists/*
```

The **`RUN`** command is used to execute commands inside the container being built. In this case is executes apt-get to install some required packages.

```Dockerfile
COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt
```

Next, the **`COPY`** command is used to copy the `requirements.txt` file into the image. The requirements file contains python libraries necessary for the app. The **RUN** command then installs the Python packages listed in that file using `pip`.

```Dockerfile
WORKDIR /
COPY ./nnUNet /nnUNet
RUN pip install -e /nnUNet
COPY . /
```

The **WORKDIR** command sets the working directory to the root directory (`/`) of the image. The **COPY** command is then used to copy the `nnUNet` directory into the image, which is installed as a Python package using `pip`. The **COPY** command is again used to copy the rest of the files in the current directory into the image.

Several environment variables are then set using the **ENV** command. Many of these variables are required for nnUNet inference, due to the CLI currently being used. The `BUILD_ENV` can set to `dev`, in which case the container will load validation data instead of running inference, saving a lot of time when debugging. By default the value will be `prod`, meaning the container will run normally.

The `ENV_SAVE_NII` variable allows saving the final label image as nii.gz format, as well as the usual dicom struct file.

```Dockerfile
# If BUILD_ENV is not set, set it to prod
ENV BUILD_ENV=${BUILD_ENV:-prod}
# If ENV_SAVE_NII is not set, set it to 0
ENV ENV_SAVE_NII=${ENV_SAVE_NII:-0}

RUN ["chmod", "+x", "/code/main.sh"]

CMD ["bash", "-c", "/code/main.sh"]
```

The **RUN** command gives executable permission to the `main.sh` script, which is then set as the entrypoint for the container when it is started from this image. Essentially, everything but the final line of the Dockerfile is setup for `main.sh`, which starts `app.py`.

## Docker container

A **Docker container** is an instance of a Docker image that is running as a standalone executable package. Containers are isolated from each other and from the host system, which makes them portable and easy to deploy across different environments.

**Running**

Use the [`docker run`](https://docs.docker.com/engine/reference/commandline/run/) command to run containers from images, locally. Build shell scripts are also available for this functionality. 

This command is only used for testing, since the images are pulled and run by the [inference server](https://github.com/mathiser/inference_server) during production. 

**Pushing**

After building a new version of a model, the new image should be pushed to [dockerhub](https://hub.docker.com/?namespace=dcpt) using [`docker push`](https://docs.docker.com/engine/reference/commandline/push/). Again, shell scripts are provided. 

The image should be pushed twice, once with the new [version number](https://semver.org/) and once with the **latest** tag. Currently, the production inference server only uses the latest version of each image, but this can be configured by posting a new model through the [API](https://dcpt-frog2.onerm.dk/docs), with the desired container tag.
