# -*- coding: utf-8 -*-
"""
Created on Mon Mar  6 13:48:00 2023

@author: helviv
"""

import logging
import os
import pickle
import numpy as np
import SimpleITK as sitk

from skimage import measure
from typing import Dict, List
import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, InputContext, IOType, Operator, OutputContext

from .timer import TimeOP
# y=0 mastectomy, y=1 lumpectomy


@md.input("img", dict, IOType.IN_MEMORY)
@md.input("mast_seg", np.ndarray, IOType.IN_MEMORY)
@md.input("lump_seg", np.ndarray, IOType.IN_MEMORY)
# @md.input("series_description", str, IOType.IN_MEMORY)
@md.output("seg", np.ndarray, IOType.IN_MEMORY)
@md.env(pip_packages=["monai", "simpleitk", "numpy", "nnunet"])
class MastLumpClassify(Operator):
    def __init__(self, split_labels: dict, model: str):
        self.logger = logging.getLogger(f"{__name__}.{type(self).__name__}")
        if os.environ['BUILD_ENV'] == "dev":
            self.logger.setLevel(logging.DEBUG)
        super().__init__()
        self.split_labels = split_labels
        self.model = model

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
    # def compute(img: np.ndarray, mast_seg: np.ndarray, lump_seg: np.ndarray) -> Tuple[int, float, float]:
        """
        Determine whether a patient had a mastectomy or lumpectomy
        Parameters:
        img (sitk.Image): CT image
        file_mast (np.ndarray): mastectomy segmentation image
        file_lu (np.ndarray): lumpectomy segmentation image

        Returns:
        int: 0 if patient had a mastectomy, 1 if patient had a lumpectomy
        """
        timer = TimeOP(__name__)

        img = op_input.get("img")
        series_description = list(img.keys())[0]#img.metadata()
        img = list(img.values())[0]#img.asnumpy()
        img = sitk.GetArrayFromImage(img)
        mast_seg = op_input.get("mast_seg")
        lump_seg = op_input.get("lump_seg")


        if "mast" in series_description.lower():
            self.logger.info("Selecting mastectomy based on series description")
            op_output.set(mast_seg)
            return
        elif "lump" in series_description.lower():
            self.logger.info("Selecting lumpectomy based on series description")
            op_output.set(lump_seg)
            return

        # Transpose all

        img = np.transpose(img, (1, 2, 0))

        frac_m, b, frac_l_m, prediction = self.determine_mast_lump(img, mast_seg, lump_seg)

        if prediction == 0:
            op_output.set(mast_seg)
            self.logger.info("Detected mastectomy")
        elif prediction == 1:
            op_output.set(lump_seg)
            self.logger.info("Detected lumpectomy")
        else:
            self.logger.error("Prediction is not 0 or 1")

        print(timer.report())

        return b, frac_m, frac_l_m, prediction

    def determine_mast_lump(self, img, mast_seg, lump_seg):
        for z in range(np.shape(img)[2]):
            img[:, :, z] = self.flip_and_rotate(img[:, :, z])

        struct_mast = self.load_struct_data(mast_seg)
        struct_lu = self.load_struct_data(lump_seg)

        # Determine if left or right

        is_left = self.determine_if_left(struct_mast)
        # if 'struct1' in struct_mast:
        bc_r, bc_l, st = self.preprocess_struct1(struct_mast)
        ############################################################################
        # RIGHT
        if is_left == False:
            # st[:, int(np.shape(struct_mast['struct1'])[1]/2):, :] = 0
            st[:int(np.shape(struct_mast['struct1'])[1]/2), :, :] = 0

            d_clip, ctv_m = self.get_ctv_m(img, struct_mast, st)
            frac_m = (ctv_m+bc_r)/bc_l
            cl = (d_clip > 4500)*1
            b = self.islands(cl)

            ctv_l = self.get_ctv_l(struct_lu)

            frac_l = (ctv_l+bc_r)/bc_l
            frac_l_m = frac_l/frac_m

        # LEFT
        if is_left == True:
            st[int(np.shape(struct_mast['struct1'])[1]/2):, :, :] = 0
            # st[:, :int(np.shape(struct_mast['struct1'])[1]/2), :] = 0

            d_clip, ctv_m = self.get_ctv_m(img, struct_mast, st)
            frac_m = (ctv_m+bc_l)/bc_r
            cl = (d_clip > 4500)*1
            b = self.islands(cl)

            ctv_l = self.get_ctv_l(struct_lu)
            frac_l = (ctv_l+bc_l)/bc_r
            frac_l_m = frac_l/frac_m

        model1=pickle.load(open(self.model, 'rb'))
        X=[b, frac_m, frac_l_m]
        X=np.array(X)
        prediction = model1.predict(X.reshape(1,-1))
        return frac_m,b,frac_l_m,prediction


    def flip_and_rotate(self, data: np.ndarray) -> np.ndarray:
        """
        Flip the data horizontally and rotate it 270 degrees.

        Parameters:
        data (numpy.ndarray): 3D numpy array

        Returns:
        numpy.ndarray: 3D numpy array after horizontal flip and 270 degree rotation
        """
        return np.rot90(np.fliplr(data), k=3)


    def load_struct_data(self, img : np.ndarray) -> Dict[str, np.ndarray]:
        """
        Load dict of all structures and rotate z axis
        Parameters:
        img (np.ndarray): path to file

        Returns:
        tuple: Tuple containing structure data and structure dictionary
        """
        struct_dict = self.open_separate(img)
        for k in struct_dict.keys():
            # Transpose
            # struct_dict[k] = np.transpose(struct_dict[k], (2, 1, 0))
            for z in range(np.shape(struct_dict[k])[2]):
                struct_dict[k][:, :, z] = self.flip_and_rotate(struct_dict[k][:, :, z])

        return struct_dict


    def get_ctv_l(self, struct_lu):
        if 'struct12' in struct_lu:
            ctv_l = np.count_nonzero(struct_lu['struct12'] != 0)
        if 'struct11' in struct_lu:
            ctv_l = np.count_nonzero(struct_lu['struct11'] != 0)
        if ('struct11' not in struct_lu) and ('struct12' not in struct_lu):
            ctv_l = 0.0000001
        return ctv_l

    def get_ctv_m(self, img, struct_mast, st):
        if 'struct12' in struct_mast:
            self.logger.info("CTV structure 12 found")
            if 'struct1' in struct_mast:
                d_clip = img*(struct_mast['struct12']+st)
            else:
                d_clip = img*(struct_mast['struct12'])
            ctv_m = np.count_nonzero(struct_mast['struct12'])
        if 'struct11' in struct_mast:
            self.logger.info("CTV structure 11 found")
            if 'struct1' in struct_mast:
                d_clip = img*(struct_mast['struct11']+st)
            else:
                d_clip = img*(struct_mast['struct11'])
            ctv_m = np.count_nonzero(struct_mast['struct11'])
        if ('struct11' not in struct_mast) and ('struct12' not in struct_mast):
            self.logger.info("No CTV structure found")
            ctv_m = 0
        return d_clip,ctv_m

    def preprocess_struct1(self, struct_mast : Dict[str, np.ndarray]):
        bc_r = np.count_nonzero(struct_mast['struct1'][int(np.shape(struct_mast['struct1'])[1]/2)-10:, :, :] != 0)
        bc_l = np.count_nonzero(struct_mast['struct1'][:int(np.shape(struct_mast['struct1'])[1]/2)+10, :, :] != 0)
        # bc_r = np.count_nonzero(struct_mast['struct1'][:, :int(
        #             np.shape(struct_mast['struct1'])[1]/2)-10, :] != 0)
        # bc_l = np.count_nonzero(struct_mast['struct1'][:, int(
        #             np.shape(struct_mast['struct1'])[1]/2)+10:, :] != 0)
        st = struct_mast['struct1']*12
        return bc_r,bc_l,st

    def determine_if_left(self, struct_mast : Dict[str, np.ndarray]) -> bool:

        """
        Returns False for right and True for left
        """
        y_p_lr = None

        if 'struct12' in struct_mast:
            structure = struct_mast['struct12']
        if 'struct11' in struct_mast:
            structure = struct_mast['struct11']

        r_ptv = structure[int(np.shape(structure)[1]/2): ,:, :]#structure[:, :int(np.shape(structure)[1]/2), :]
        l_ptv = structure[: int(np.shape(structure)[1]/2),:, :]#structure[:, int(np.shape(structure)[1]/2):, :]
        if np.count_nonzero(r_ptv == 0) < np.count_nonzero(l_ptv == 0):
            y_p_lr = False
        else:
            y_p_lr = True

        return y_p_lr


    def islands(self, structure : np.ndarray) -> int:

        # Find the labeled regions in the volume
        labels = measure.label(structure)
        
        # Find the properties of each labeled region
        properties = measure.regionprops(labels)
        
        # Return the number of labeled regions
        return len(properties)


    def open_separate(self, data: np.ndarray) -> Dict[str, np.ndarray]:
        """

        Create dictionary with binary arrays for each unique element in input array

        Parameters:
        data (numpy.ndarray): 3D numpy array

        Returns:
        dict[numpy.ndarray]: dict of binary arrays with key 'struct#' where # is a value from the input array

        """

        res = np.unique(data)

        structs_dict = {f"struct{int(i)}" : np.where(data == i, i, 0) for i in res}

        return structs_dict
    
if __name__ == '__main__':
    os.environ['BUILD_ENV'] = 'dev'
    class_op = MastLumpClassify(split_labels={"eqwsa": 5}, model='/home/frog/mathis/segmentation-models/mamma_ct_oars_mastlump/img/model/LumpMast_Classifier/forest.sav')

    img = np.transpose(sitk.GetArrayFromImage(sitk.ReadImage('/home/frog/docker_test/out_mamma/scan.nii.gz')), (1, 2, 0))
    # import nibabel as nib
    # img = nib.load('/home/frog/docker_test/out_mamma/scan517.nii.gz').get_fdata()
    img_mast = sitk.GetArrayFromImage(sitk.ReadImage('/home/frog/docker_test/out_mamma/scan518.nii.gz'))
    img_lump = sitk.GetArrayFromImage(sitk.ReadImage('/home/frog/docker_test/out_mamma/scan521.nii.gz'))

    class_op.determine_mast_lump(img=img, mast_seg=img_mast, lump_seg=img_lump)