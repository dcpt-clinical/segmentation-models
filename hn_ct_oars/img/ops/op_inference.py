import logging
import os
import tempfile
import SimpleITK as sitk
import numpy as np

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, InputContext, IOType, Operator, OutputContext

from .timer import TimeOP


@md.input("img", sitk.Image, IOType.IN_MEMORY)
@md.output("seg", np.ndarray, IOType.IN_MEMORY)
@md.env(pip_packages=["monai", "simpleitk", "numpy", "nnunet"])
class Predict(Operator):
    def __init__(self, task_id: str):
        self.logger = logging.getLogger("{}.{}".format(__name__, type(self).__name__))
        super().__init__()
        self.task_id = task_id

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):

        timer = TimeOP(__name__)
        img = op_input.get("img")

        if os.environ['BUILD_ENV'] == "dev":
            sitk.WriteImage(img, os.path.join(os.environ["OUTPUT"], "scan.nii.gz"))
            #pred_arr = np.load(os.path.join(os.environ['VALIDATION'], 'pred.npy'))
        # else:
        pred_arr = self.predict(img)
        op_output.set(pred_arr, "seg")

    def predict(self, img):
        try:
            with tempfile.TemporaryDirectory() as tmp_in, tempfile.TemporaryDirectory() as tmp_out:
                sitk.WriteImage(img, os.path.join(tmp_in, "tmp_0000.nii.gz"))
                print("det er en rigtig aendring")
                os.system(f"nnUNet_predict -t {self.task_id} -tr nnUNetTrainerV2 -f 0 -i {tmp_in} -o {tmp_out}")

                for f in os.listdir(tmp_out):
                    if f.endswith(".nii.gz"):
                        pred_img = sitk.ReadImage(os.path.join(tmp_out, f))
                        pred_img.CopyInformation(img)
                        pred_arr = sitk.GetArrayFromImage(pred_img)
                        return pred_arr

                else:
                    raise Exception("No prediction found")

        except Exception as e:
            self.logger.error(e)
            raise e

        print(timer.report())
