import json
import os.path

from monai.deploy.core import Application

from ops.op_postprocessing import SeparateStructure
from ops.op_inference import Predict
from ops.op_reader import DataLoader
from ops.op_writer import DataWriter


class InferenceApplication(Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compose(self):
        dataloader_op = DataLoader()
        meta_info = self.load_meta_info("meta_info.json")
        meta_info_511 = self.load_meta_info("meta_info_511.json")
        meta_info_512 = self.load_meta_info("meta_info_512.json")
        meta_info_513 = self.load_meta_info("meta_info_513.json")
        meta_info_514 = self.load_meta_info("meta_info_514.json")

        predict_op = Predict(tasks=meta_info["tasks"])

        postprocessing_511_op = SeparateStructure(split_labels=meta_info_511["split_labels"])
        postprocessing_512_op = SeparateStructure(split_labels=meta_info_512["split_labels"])

        writer_op = DataWriter(meta_511_info=meta_info_511,
                               meta_512_info=meta_info_512,
                               meta_513_info=meta_info_513,
                               meta_514_info=meta_info_514)

        # Flows
        self.add_flow(dataloader_op, predict_op, {"img": "img"})

        # Postprocess 511 and 512
        self.add_flow(predict_op, postprocessing_511_op, {"511": ""})
        self.add_flow(predict_op, postprocessing_512_op, {"512": ""})

        # Write all
        self.add_flow(dataloader_op, writer_op, {"dcm_dir": "dcm_dir"})
        self.add_flow(postprocessing_511_op, writer_op, {"": "511"})
        self.add_flow(postprocessing_512_op, writer_op, {"": "512"})
        self.add_flow(predict_op, writer_op, {"513": "513",
                                              "514": "514"})

    @staticmethod
    def load_meta_info(path):
        with open(path, "r") as r:
            meta_info = json.loads(r.read())
        return meta_info


if __name__ == "__main__":
    InferenceApplication(do_run=True)
