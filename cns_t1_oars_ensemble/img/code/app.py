import json
import os.path

from monai.deploy.core import Application

from ops.op_postprocessing import SeparateStructure
from ops.op_inference import Predict
from ops.op_reader import DataLoader
from ops.op_evaluator import Evaluator
from ops.op_writer import DataWriter

class InferenceApplication(Application):
    '''
    This is the main application class for the inference pipeline.
    It differs from the other applications in that all data is passed as dicts of arrays, with the keys being the task id.
    This makes adding new tasks easier, as the application does not need to be modified,
    all that is needed is a new meta_info file as well as a new model.
    However, this also means that the application is not as efficient as it could be about memory usage,
    and this way of structuring the application sort of goes against the monai.deploy philosophy.
    '''
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compose(self):
        dataloader_op = DataLoader()

        meta_info = [
            self.load_meta_info(filename) 
            for filename 
            in os.listdir('/cfg') 
            if filename.startswith("meta_info_") 
            and filename.endswith(".json")]

        predict_op = Predict(tasks=meta_info)
        postprocessing_op = SeparateStructure(tasks_info=meta_info)
        evaluator_op = Evaluator(meta_info=meta_info)
        writer_op = DataWriter(meta_info=meta_info)

        # Flows
        self.add_flow(dataloader_op, predict_op, {"img": "img"})
        self.add_flow(dataloader_op, writer_op, {"dcm_dir": "dcm_dir"})
        self.add_flow(dataloader_op, evaluator_op, {"dcm_dir": "dcm_dir"})

        self.add_flow(predict_op, postprocessing_op, {"predictions": "predictions"})
        self.add_flow(postprocessing_op, evaluator_op, {"pp_arrs": "pp_arrs"})
        self.add_flow(evaluator_op, writer_op, {"evals": "evals"})
        self.add_flow(postprocessing_op, writer_op, {"pp_arrs": "pp_arrs"})

    @staticmethod
    def load_meta_info(path):
        with open(os.path.join('/cfg', path), "r") as r:
            meta_info = json.loads(r.read())
        return meta_info

if __name__ == "__main__":
    InferenceApplication(do_run=True)
