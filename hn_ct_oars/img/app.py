import json
import os.path
from monai.deploy.core import Application

from ops.op_postprocessing import SeparateStructure
from ops.op_inference import Predict
from ops.op_reader import DataLoader
from ops.op_writer import DataWriter


class InferenceApplication(Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compose(self):
        with open("meta_info.json", "r") as r:
            meta_info = json.loads(r.read())

        dataloader_op = DataLoader()
        predict_op = Predict(task_id=meta_info["task_id"])
        postprocessing_op = SeparateStructure(split_labels=meta_info["split_labels"])
        writer_op = DataWriter(final_contours=meta_info["final_contours"],description=meta_info["model_name"])

        # Flows
        self.add_flow(dataloader_op, predict_op, {"img": "img"})
        self.add_flow(predict_op, postprocessing_op, {"seg": "seg"})
        self.add_flow(dataloader_op, writer_op, {"dcm_dir": "dcm_dir"})
        self.add_flow(postprocessing_op, writer_op, {"seg": "seg"})

if __name__ == "__main__":
    InferenceApplication(do_run=True)

