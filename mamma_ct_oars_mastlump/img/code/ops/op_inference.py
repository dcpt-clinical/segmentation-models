import logging
import os
import tempfile
from typing import Dict, List
import SimpleITK as sitk
import numpy as np

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, InputContext, IOType, Operator, OutputContext

from .timer import TimeOP


@md.input("img", dict, IOType.IN_MEMORY)
@md.output("seg", np.ndarray, IOType.IN_MEMORY)
# @md.output("folds", Dict[np.ndarray], IOType.IN_MEMORY)
@md.env(pip_packages=["monai", "simpleitk", "numpy", "nnunet"])
class Predict(Operator):
    '''
    Predicts the segmentation for the given image and task.
    Runs ensembling if multiple folds specified in the provided task/meta_info
    '''
    def __init__(self, task: dict = None):
        self.logger = logging.getLogger(f"{__name__}.{type(self).__name__}")
        if os.environ['BUILD_ENV'] == "dev":
            self.logger.setLevel(logging.DEBUG)
        super().__init__()
        self.task = task

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        timer = TimeOP(__name__)
        img = op_input.get("img")
        
        img = list(img.values())[0]
        predictions = {}

        # Dev build skips inference and loads precomputed predictions
        # Requires a mamma scan, which is not currently available
        # if os.environ['BUILD_ENV'] == "dev":
        # sitk.WriteImage(img, os.path.join(os.environ["OUTPUT"], "scan.nii.gz"))
        #     predictions['prediction'] = np.load(os.path.join(os.environ['VALIDATION'], self.task['id'] + '_pred.npy'))
        #     if len(self.task['folds']) > 1:
        #         predictions['folds'] = {
        #             f_id: 
        #             np.load(os.path.join(os.environ['VALIDATION'], f'{self.task["id"]}_fold_{f_id}_pred.npy')) 
        #             for f_id 
        #             in self.task['folds']}
        #     else:
        #         predictions['folds'] = None
        # else:
        # try:
        #     predictions['prediction'] = sitk.GetArrayFromImage(sitk.ReadImage(os.path.join(os.environ["OUTPUT"], f"scan{self.task['id']}.nii.gz")))
        # except:
        predictions['prediction'], predictions['folds'] = self.predict(img, task_id=self.task["id"], threshold=self.task["threshold"], folds=self.task["folds"], inference_args=self.task["inference_args"])

        op_output.set(predictions['prediction'], "seg")
        # if predictions['folds']:
        #     op_output.set(predictions['folds'], "folds")

        print(timer.report())

    # This methods performs inference using a nnUNet task, specified by the task_id parameter
    # The image is saved to a temporary directory and passed to the nnUNet_predict command
    # If the parameter threshold is true, a binary image is created as an additional input to the task
    # If more than one folds is specified, the folds are predicted separately and then ensembled
    # The inference_args parameter is used to pass additional arguments to the nnUNet_predict command, such as --disable_tta
    # The method returns the prediction and a dictionary of the predictions for each fold
    def predict(self, img: sitk.Image, task_id: str, threshold: bool, folds=None: list, inference_args=None: str):
        with tempfile.TemporaryDirectory() as tmp_in, tempfile.TemporaryDirectory() as tmp_out:
            sitk.WriteImage(img, os.path.join(tmp_in, "tmp_0000.nii.gz"))
            if threshold:
                bin_img = self.make_binary_image(img, 130, 1500)
                sitk.WriteImage(bin_img, os.path.join(tmp_in, "tmp_0001.nii.gz"))

            if len(folds) == 1:
                self.logger.info(f"Predicting for task {task_id} with threshold {threshold}")
                fold_path = os.path.join(tmp_out, f"Fold_{folds[0]}_results")
                os.makedirs(fold_path, exist_ok=True)
                os.system(f"nnUNet_predict -t {task_id} -tr nnUNetTrainerV2 -f {folds[0]} -i {tmp_in} -o {fold_path} --save_npz {inference_args}")
                return self.read_prediction(fold_path, img), None
            else:
                results = dict()
                fold_paths = []
                for count, fold in enumerate(folds):
                    self.logger.info(f"Predicting fold {fold} (#{count+1} out of {len(folds)}) for self.task {task_id} with threshold: {threshold}")
                    fold_path = tempfile.mkdtemp(dir=tmp_out, prefix=f"Fold_{fold}_")
                    os.makedirs(fold_path, exist_ok=True)
                    fold_paths.append(fold_path)
                    os.system(f"nnUNet_predict -t {task_id} -tr nnUNetTrainerV2 -f {fold} -i {tmp_in} -o {fold_path} --save_npz {inference_args}")
                    results[fold] = (self.read_prediction(fold_path, img))

                # Ensemble predictions
                ensemble_path = tempfile.mkdtemp(dir=tmp_out, prefix=f"{task_id}_ensemble_results")
                self.logger.info(f"Ensembling {len(folds)} folds for self.task {task_id} with threshold {threshold}")
                os.system(f"Command:\r\nnnUNet_ensemble -f {' '.join(fold_paths)} -o {ensemble_path}")

                
                return self.read_prediction(ensemble_path, img), results

    @staticmethod
    def read_prediction(path, img):
        for f in os.listdir(path):
            if f.endswith(".nii.gz"):
                pred_img = sitk.ReadImage(os.path.join(path, f))
                pred_img.CopyInformation(img)
                pred_arr = sitk.GetArrayFromImage(pred_img)
                pred_arr = np.transpose(pred_arr, (1, 2, 0))
                return pred_arr
        else:
                raise Exception("No prediction found")

    def make_binary_image(self, img: sitk.Image, lower_thresh, upper_thresh):
        arr = sitk.GetArrayFromImage(img)
        bin_arr = np.where(np.logical_and(np.greater_equal(arr, lower_thresh), np.greater_equal(upper_thresh, arr)), 1,
                           0)
        bin_img = sitk.GetImageFromArray(bin_arr)
        bin_img.CopyInformation(img)
        return bin_img
